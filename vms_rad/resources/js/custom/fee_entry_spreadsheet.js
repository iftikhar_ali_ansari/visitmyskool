$(document).ready(function() {
	
	var $container = $("#fee_entry_spreadsheet");
	var $parent = $container.parent();
	var student_id = 10;
	var feeDescription = Array();
	var fee_description_amount_mapping_stack = Array();
	var fee_description_fee_structure_mapping_stack = Array();
	
	var oTable2 = $('#payment_history').dataTable({
		"bProcessing":false,
		"bServerSide":true,
		"sPaginationType":"full_numbers",		
		"bPaginate": true,
		"bJQueryUI": true,								
		"sAjaxSource": _baseUrl+"blocks/generate_payment_history_table_block",
		"aoColumns": [
				null,
				null,
				null,
				null
		],
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			$.ajax({
				"dataType": 'json',
				"type": "POST",
				"url": sSource,
				"data": aoData,
				"success": fnCallback
			});
		},		
		"aaSorting": [[ 0, 'asc' ]],
		"iDisplayLength": 5,
		"bLengthChange": false,
		"bFilter": false,
		"bInfo": false
	});

	
	$.post(_baseUrl+"admin/fetch_pending_payments/", { student_id: student_id },
		function(response) {
			
			//Building the fee description drop-down list
			var response_length = response.length;
			var response_counter = 1;
			while(response_counter < response_length){
				feeDescription.push(response[response_counter]);
				response_counter = response_counter + 3;
			}
			
			//Building the key-value pair of each fee description
			var response_length = response.length;
			var response_counter = 0;
			while(response_counter < response_length){
				fee_description_amount_mapping_stack[response[response_counter+1]] = response[response_counter+2];
				response_counter = response_counter + 3;
			}			
			
			//Building the key-value pair of each fee structure id
			var response_length = response.length;
			var response_counter = 0;
			while(response_counter < response_length){
				fee_description_fee_structure_mapping_stack[response[response_counter+1]] = response[response_counter];
				response_counter = response_counter + 3;
			}
			
		},
	"json");
	
	$container.handsontable({
	  startRows: 8,
	  startCols: 3,
	  colWidths: [1, 250, 50],
	  stretchH: 'all',
	  rowHeaders: true,	  
	  colHeaders: true,
	  minSpareRows: 1,
	  contextMenu: false,
	  colHeaders: ["ID", "Description", "Amount"],
	  columns: [
	        {
	          readOnly: true
	        },
	        {
			  type: "autocomplete",
			  source: feeDescription, 
			  strict:true
			},
			{
			  readOnly: true
			}
		],
		afterChange: function(changes, secondarySource) {
			var ht = $container.handsontable('getInstance');
			if(changes != null) {
				var myarray = changes[0];
				//If it's the 0th column
				if(myarray[1] == 1) {
					//myArray[0] would output the corresponding row number; column hard-coded to 0
					var key = ht.getDataAtCell(myarray[0],1);
					//Add value to the Amount column; Value of the mapping stack's key
					ht.setDataAtCell(myarray[0], 2, fee_description_amount_mapping_stack[key]);
					ht.setDataAtCell(myarray[0], 0, fee_description_fee_structure_mapping_stack[key]);
				}
			}					    
		}
	});
	var handsontable = $container.data('handsontable');

	$parent.find("#save_fee_entry").click(function () {
		$(this).html("<i class='icon-refresh'>&nbsp;</i>&nbsp;Saving...");
		console.log(handsontable.getData());
		$.ajax({			
			url: _baseUrl+"admin/save_fee_entry",
			data: {"data": handsontable.getData()}, //returns all cells' data
			dataType: 'json',
			type: 'POST',		
			success: function (res) {
				if (res.result === 'ok') {
					console.log('Data saved');
					oTable2.fnDraw();
					$("#save_fee_entry").html("<i class='icon-save'>&nbsp;</i>&nbsp;Save");
					$("#fee_entry_spreadsheet").handsontable("clear");
				}
				else {
					console.log('Save error');
				}
			},
			error: function () {
				console.log('Save error. POST method has encountered an error.');
			}
		});
	});
	
	$parent.find("#cancel_fee_entry").click(function(){
		$("#fee_entry_spreadsheet").handsontable("clear");
	});
	
});
