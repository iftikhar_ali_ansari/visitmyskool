$("document").ready(function(){
	
	var oTable = $('#students').dataTable({
		"bProcessing":false,
		"bServerSide":true,
		"bPaginate": true,
		"bJQueryUI": true,
		"sPaginationType":"two_button",
		"sAjaxSource": _baseUrl+"blocks/generate_students_table_block",
		"aoColumns": [
				null,
				null
		],
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			$.ajax({
				"dataType": 'json',
				"type": "POST",
				"url": sSource,
				"data": aoData,
				"success": fnCallback
			});
		},		
		"aaSorting": [[ 0, 'asc' ]],
		"iDisplayLength": 14,
		"bLengthChange": false,
		"bFilter": true,
		"bInfo": false,
		"oLanguage": {"sSearch": ""}
	});
	
	$(".dataTables_filter input").css("display", "none");
	
	$("#search-query").keyup( function () {
	  oTable.fnFilter( this.value );
	});

	$("#students tbody").on( "click", "td", function () {	  
		if ( $(this).index() === 1 ) {
			var rowData = oTable.fnGetData( this.parentNode );
			$(this).append("<i class='icon-chevron-right pull-right'>&nbsp;</i>");
			var student_id = rowData[ rowData.length-2 ];
			$("#student_profile").removeClass("icon-user");
			$("#student_profile").addClass("icon-refresh");
			$.post(_baseUrl+"admin/fetch_student_details/", { student_id: student_id },
				function(response) {
					var student_details = response.student_data[0];
					$("#student_profile").removeClass("icon-refresh");
					$("#student_profile").addClass("icon-user");
					$("#selected_student_name").html(student_details['first_name']+" "+student_details['last_name']);
					$("#selected_student_reg_no").html(student_details['student_id']);
					$("#selected_student_father_name").html(student_details['fahter_name']);
					$("#selected_student_mother_name").html(student_details['mother_name']);
				},
			"json");
		}
	});
	
	$("#students_previous").html("<i class='icon-chevron-left'></i>");
	$("#students_next").html("<i class='icon-chevron-right'></i>");
		
});