(function($) {

    $.fn.initialize = function($month,$id,$data,$year) {
	    // the month is for telling which data to be updated because full month is in two table on view
		// the id holds the id of the months
		// the data hold the the attendance detail fetched from the table in json form
        $status = "present"; // status can be present absent and interval(holiday)
		$presentDay = 1; // for calculating the total day in the month we need the day 1 of the month
		$(this).empty();
		$date = new Date();
		$date2 = new Date();
		$date1 = new Date();
		$current_month = 6; // default value for the current month if month is not passed the current month will be 6
		$date.setMonth($current_month);
		$date2.setMonth($current_month+1);
		$date1.setMonth($current_month);
		$date1.setDate(1);
		$presentDay = $date1.getDay();
		$current_year = 2013;
		        if($year) $current_year = $year;
				  
				$date.setFullYear($current_year); // constant day for 2013 until fetched from the database
				$date2.setFullYear($current_year);
				
				// deciding which month is passed from the view
				if($id){
				    $current_month = $("#"+$id).text().toLowerCase();
				    if($current_month == "jan") {
				    $current_month = 0;
				}else if($current_month == "feb") {
				    $current_month = 1;
				}else if($current_month == "march") {
				    $current_month = 2;
				}else if($current_month == "april") {
				    $current_month = 3;
				}else if($current_month == "may") {
				    $current_month = 4;
				}else if($current_month == "june") {
				    $current_month = 5;
				}else if($current_month == "july") {
				    $current_month = 6;
				}else if($current_month == "aug") {
				    $current_month = 7;
				}else if($current_month == "sep") {
				    $current_month = 8;
				}else if($current_month == "oct") {
				    $current_month = 9;
				}else if($current_month == "nov") {
				    $current_month = 10;
				}else if($current_month == "dec") {
				    $current_month = 11;
				}

				$date.setMonth($current_month);
				$date2.setMonth($current_month+1);
				$date1.setMonth($current_month);
				$date1.setDate(1);
		        $presentDay = $date1.getDay();
				}
				
		if($month) {
		    // here date means first 15 days and the remaining will be datanext
			// date is the actual date of the months
			// if $month is undefine means we were working for the attendance status
		    if($month == 'date') {
			    return this.each( function() {
                    	
			        for($i=1; $i<=15;$i++)
			        $(this).append("<th>"+$i+"</th>");
				});
			}
			else if($month == 'datenext') {
			    
			    return this.each( function() {
				   
			        for($i=16;$i<=(($date2-$date)/(24*60*60*1000));$i++)
			        $(this).append("<th>"+$i+"</th>");
				});
			} else {
		    return this.each( function() {
			    // this will create the attendance status 
			    for(var $i =16; $i<=(($date2-$date)/(24*60*60*1000));$i++){
				    
				    if(isSunday($i)){
					    $status = "interval";
					}else {if($data[$i]) {
					    if($data[$i]['attendance_status']==1) {
						    $status="present";
						}else {
						    $status="absent";
						}
						} else $status="present";
					}
			        $(this).append("<td  class='"+$status+"'><div class='row-fluid lecture' ></div></td>");
				}
			
            });
			}
		} else {
        return this.each( function() {
		       
			    for(var $i =1; $i<=15;$i++){
				    
				    if(isSunday($i)){
					    $status = "interval";
					}else { if($data[$i]) {
					    if($data[$i]['attendance_status']==1) {
						    $status="present";
						}else {
						    $status="absent";
						}
						}
					}
			        $(this).append("<td  class='"+$status+"'><div class='row-fluid lecture' ></div></td>");
				}
			
        });
		}
		function isSunday($date) {
		    if(($date+$presentDay-1)%7 == 0 ){
			    
		        return true;
			}else {
			    
			    return false;
			}
		}

    }
	

}(jQuery));