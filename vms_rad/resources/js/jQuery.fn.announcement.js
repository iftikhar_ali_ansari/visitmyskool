(function($) {

    $.fn.updateAnnouncement = function($announcement) {
	    return this.each(function () {
		    alert($announcement[0]['announcement_type_id']);
			$announcement_type = '';
			
			$string = "";
			for($i=0;$i<$announcement.length;$i++) {
			    switch($announcement[$i]['announcement_type_id']) {
			    case 1:
				    $announcement_type = "default";
					break;
				case 2:
				    $announcement_type = "alert";
					break;
				case 3:
				    $announcement_type = "info";
					break;
				case 4:
				    $announcement_type = "warning";
					break;
			    }
                $string += "<div class='alert-message '"+$announcement_type+"'>"+
				    "<p><strong>Oh snap!</strong> "+$announcement[$i]['announcement']+" <a href='#'>notice</a>.</p>"+
				    "</div>";
			}
			$(this).empty();
			$(this).append($string);
        });	    
	}
}(jQuery));