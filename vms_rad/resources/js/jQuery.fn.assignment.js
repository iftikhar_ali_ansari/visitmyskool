(function($) {

    $.fn.updateAssignment = function($assignment) {
	    return this.each(function () {
			$string = "";
			for($i=0;$i<$assignment.length;$i++) {
			    
                $string += "<tr>"+
						"<td><i class='icon-check-empty'>&nbsp;</i></td>"+
						"<td>"+$assignment[$i]['assignment']+"</td>"+
					  "</tr>";
			}
			$(this).empty();
			$(this).append($string);
        });	    
	}
}(jQuery));