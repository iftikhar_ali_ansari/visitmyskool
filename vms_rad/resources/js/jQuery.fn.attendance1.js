(function($) {

    $.fn.initialize = function($day) {
        $status = "present";
		$presentDay = 0;
		if($day) $presentDay = $day;
        return this.each( function() {
			    for(var $i =1; $i<=15;$i++){
				    if(isSunday($i)){
					    $status = "interval";
					}else {
					    $status = "present";
					}
			        $(this).append("<td  class='"+$status+"'><div class='row-fluid lecture' ></div></td>");
				}
			
        });
		
		function isSunday($date) {
		    if($date%7 == 0 ){
			    
		        return true;
			}else {
			    
			    return false;
			}
		}

    }

}(jQuery));