(function($) {
    $.fn.saveAttendance = function($attendanceTable,$url) {
	    
		$row = 1;
		$col = 1;
		$i=0;
		$student_name = [];
		$index =[];
		$($attendanceTable+" tr:first").each(function() {
		    $(this).children("th").each(function() {
			    $index.push($(this).html());
			});
		});
	    $($attendanceTable+" tr:not(:first-child)").each(function() {
			$status = [];
		    $status.push($(this).children("td:first").text());
			$i++;
		    $(this).children("td").each(function () {
			    if($(this).css('background-color')=='rgb(37, 116, 50)') {
				    $status.push(1);
				}
				else if($(this).css('background-color')=='rgb(217, 35, 15)') {
				    $status.push(2);
				}
				
			});
			$student_name.push($status);
		});
		
		$.ajax({
	            url: $url,
		        type: "get",
		        datatype: "html",
				data: {"student_name":$student_name,"index":$index},
		        async:false,
		        success: function(data) {
		            //$("#display_result").append(data);
					alert(data); // if successfully saved will alert whith sucessfully saved
		        }
	        });
	}
}(jQuery));