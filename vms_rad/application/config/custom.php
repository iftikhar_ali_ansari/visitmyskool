<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['site_title'] = 'Visitmyskool.com';
$config['site_description'] = 'Online School Management Simplified';
$config['site_author'] = 'Prerana Consulting';
$config['site_keywords'] = 'key1, key2';


/* End of file custom.php */
/* Location: ./application/config/custom.php */