<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function is_duedate_crossed($due_date){
	$flag = true;
    if($due_date > new DateTime()){
    	$flag = false;
    }
    return $flag;
}

function get_today_date(){
	return date("Y-m-d");
}

?>