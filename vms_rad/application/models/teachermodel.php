<?php
  class Teachermodel extends CI_Model {
          function __construct() {
        parent::__construct();
    }
	
	function saveAttendance($query) {
	    //$this->db->query('insert into attendance(student_id,attendance_date,period_number,group_id,attendance_status,created_by) values(?,?,?,?,?,?) on duplicate key update student_id = values(student_id),attendance_date = values(attendance_date),period_number = values(period_number),group_id = values(group_id),attendance_status = values(attendance_status),created_by = values(created_by)',$query);
		
		$string ="replace into attendance(student_id,attendance_date,period_number,shift,Group_id,attendance_status,created_by) values";
		//print_r($query);
		
		foreach($query as $value)
		{
		    $string .="(";
		    $string .=$value['student_id'].",";
			$string .="'".$value['attendance_date']."'".",";
			$string .=$value['period_number'].",";
			$string .=$value['shift'].",";
			$string .=$value['Group_id'].",";
			$string .=$value['attendance_status'].",";
			$string .=$value['created_by'];
			$string .="),";
		}
		$string = substr($string,0,-1); 
		$this->db->query($string);
		//print_r($string);
		echo 'successfully updated';
	}
	
	function fetch_attendance_status($class,$start_date) {
	    $current_date = Date('Y-n-').$start_date;
		$end_date = Date('Y-n-').($start_date+5);
	    $query = $this->db->query("select person.first_name as name, student.student_id as id, attendance.attendance_status as status,attendance.attendance_date as date from person,student,attendance where person.person_id = student.student_id and attendance.student_id = student.student_id and attendance.attendance_date between '".$current_date."' and '".$end_date."' order by student.student_id, attendance.attendance_date, attendance.shift");
		return $query->result();
	}
	
	function save_announcement($announcement,$parent_check,$class=27) {
	    ($parent_check)? $parent_check = 1:$parent_check = 0;
	    $data = array (
		    'announcement_type_id'=> 1,
			'group_id'=> $class,
			'announcement'=>$announcement,
			'announced_by'=> 2,
			'parent_message_sent'=>$parent_check
		);
		$this->db->insert('announcement',$data);
	}
	
	function save_assignment($assignment,$class=27) {
	    $date = Date('Y-n-d H:i:s');
	    $data = array (
		    'assignment'=> $assignment,
			'group_id'=> $class,
			'assigned_by'=> 2,
			'assigned_date'=> $date,
			'subject_id'=> 2
		);
		$this->db->insert('assignment',$data);
	}
	
	function fetch_staff_detail($staff_id) {
	    $this->db->select("person.first_name as first_name, person.last_name as second_name, school.long_name as school_name",FALSE)
		->from("person, school_staff, school",False)
		->where("school_staff.staff_id","person.person_id",FALSE)
		->where("school_staff.school_assigned_id","school.school_id",FALSE)
		->where("school_staff.staff_id",$staff_id,FALSE);
		$result = $this->db->get();
		return $result->row();
	}
}
?>