<?php
class Adminfeesmodel extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    
	public function fetch_student_defaults($student_id) {
		$this->db->select("first_name, last_name, student_id, father_name, mother_name",FALSE)
		->from("person, student")
		->where("student.student_id","person.person_id",FALSE)
		->where("person.active_flag","Y")
		->where("student.student_id",$student_id);
		$query = $this->db->get();
		return $query->result();
	}		

	public function fetch_student_balance($student_id) {
		$this->db->select("fee_structure_detail_id, fee_description, fee_amount, due_date",FALSE);
		$this->db->from("fee_structure_detail, student_fee_mapping");
		$this->db->where("fee_structure_detail.fee_structure_id","student_fee_mapping.fee_structure_id",FALSE);
		$this->db->where("student_fee_mapping.student_id",$student_id);
		$query = $this->db->get();
		return $query->result();
	}
	
	public function fetch_student_balance_total($student_id) {
		$total = 0.00;
		$this->db->select("SUM(fee_amount) as total",FALSE);
		$this->db->from("fee_structure_detail, student_fee_mapping");
		$this->db->where("fee_structure_detail.fee_structure_id","student_fee_mapping.fee_structure_id",FALSE);
		$this->db->where("student_fee_mapping.student_id",$student_id);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach ($query->result() as $row)
			{
				$total = $row->total;
			}
		}
		return $total;
	}

	public function fetch_reciept_no($school_id) {
		$this->db->select("MAX(fee_reciept_code) as reciept_no",FALSE);
		$this->db->from("fee_payment");
		$this->db->where("school_id",$school_id);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach ($query->result() as $row)
			{
				$reciept_no = $row->reciept_no;
			}
		}
		$reciept_no = $reciept_no + 1;
		return $reciept_no;
	}

	public function fetch_pending_payments($student_id) {
		$this->db->select("fee_structure_detail.fee_structure_detail_id, fee_structure_detail.fee_description, fee_structure_detail.fee_amount",FALSE);
		$this->db->from("fee_structure_detail, student_fee_mapping, fee_payment");
		$this->db->where("fee_structure_detail.fee_structure_id","student_fee_mapping.fee_structure_id",FALSE);
		$this->db->where("fee_payment.fee_structure_detail_id <>","fee_structure_detail.fee_structure_detail_id",FALSE);
		$this->db->where("student_fee_mapping.student_id",$student_id);
		$query = $this->db->get();
		foreach($query->result_array() as $row)
		{
			$data[] = $row;
		}
		return $data;
	}
	
	public function save_fee_payment($fee_payment_data)	{
		$query_exe_status = false;
		if($this->db->insert("fee_payment", $fee_payment_data)){
			$query_exe_status = true;
		}
		return $query_exe_status;
	}
	
	public function fetch_student_marks($student_id){
	    return True;
	}
	
	public function fetch_staff_details($staff_id) {
	    return True;
	}
	
	public function fetch_school($school_id) {
	    return True;
	}
	
	public function fetch_class() {
	    return True;
	}
	
	public function fetch_exam_type() {
	    return True;
	}
	
	public function fetch_student_names() {
	    $result = $this->db->query("select student_id from student");
		//print_r($result->result());
		
		for($i=1;$i<11;$i = $i+9) {
		    $query[] = $this->db->query("select marks from marks where student_id=".$i);
		}
		//print_r($query->result());
		for($i=0;$i<2;$i++){
		foreach ($query[$i]->result() as $row) {
		    printf("<br>".$i." ");
		    print_r($row->marks);
		}
		}
		/*
	   $this->db->select("concat(person.first_name,' ',person.last_name) as name,marks.subject_id as subject,marks.marks as marks",FALSE)
	   ->from("student,person,marks")
	   ->where("student.student_id","person.person_id",FALSE)
	   ->where("student.student_id","marks.student_id limit 10",FALSE);
	   $query = $this->db->get();
	   return $query->result(); */
	}
	
	public function add_reccord($data)
{
	$this->db->insert('new_student',$data);
/*$array1=array( 'ADDRESS_1' => $this->input->post('Address'));
		$this->db->insert('contact',$array1);

$this->db->select('CONTACT_ID');
$query = $this->db->get_where('contact', array('ADDRESS_1' => $array1['']));

$array2=array( 'FIRST_NAME' => $this->input->post('First_name'),
				'LAST_NAME' => $this->input->post('Last_name'));
$this->db->where('CONTACT_ID', $query);
$this->db->update('person', $array2); 

$this->db->select('PERSON_ID');
$query1 = $this->db->get_where('person', array('LAST_NAME' => $data['Last_name']));

$array3=array( 'FATHER_NAME' => $this->input->post('Father_name'),
				'MOTHER_NAME' => $this->input->post('Mother_name'));
$this->db->where('CONTACT_ID', $query1);
$this->db->update('student', $array3); 

*/

	return ;
}

}
