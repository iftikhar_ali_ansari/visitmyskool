<?php
  class Studentmodel extends CI_Model {
          function __construct() {
        parent::__construct();
    }
	
	public function fetch_student_defaults($student_id) {
		$this->db->select("first_name, last_name, student_id, father_name, mother_name",FALSE)
		->from("person, student")
		->where("student.student_id","person.person_id",FALSE)
		->where("person.active_flag","Y")
		->where("student.student_id",$student_id);
		$query = $this->db->get();
		return $query->row();
	}
  
  
  public function fetch_contact_detail($student_id) {
	  $this->db->select("primary_contact_person")
	  ->from("student")
	  ->where("student_id",$student_id);
	  $person = $this->db->get();
	  //echo 'in the student model';
	  $primary_cont = $person->row();
	  
	  //print_r($primary_cont[0]->primary_contact_person);
	  $this->db->select("address_1,address_2,city,state,country,landline,primary_mobile",FALSE)
	  ->from("person,contact")
	  ->where("person.contact_id","contact.contact_id",FALSE)
	  ->where("person.contact_id",$primary_cont->primary_contact_person);
	  $query = $this->db->get();
	  //print_r($query);
	  return $query->row();
  }
  
  public function fetch_school_detail($student_id) {
      $this->db->select("long_name,short_name,school_code",FALSE)
	  ->from("student,school")
	  ->where("student.school_id","school.school_id",FALSE)
	  ->where("student.student_id",$student_id);
	  $query = $this->db->get();
	  return $query->row();
  }
  
  public function fetch_attendance_detail($student_id) {
      $query = $this->db->query("select count(attendance_date) as total_present from attendance where attendance_status=1 and student_id=".$student_id);
	  return $query->row();
  }
  
  public function fetch_total_working_day($student_id) {
      $query = $this->db->query("select count('attendance_date') as total_working_days from attendance where student_id=".$student_id);
      return $query->row();
  }
  
  public function fetch_test_data() {
      $query = $this->db->query("select attendance_status,Extract(Year From attendance_date) from attendance where student_id=1");
	  return $query->result();
  }
  
  public function fetch_max_min_date() {
      $query = $this->db->query("select extract(year from max(attendance_date))as max, extract(year from min(attendance_date)) as min from attendance");
	  return $query->row();
  }
  
  public function fetch_announcement($student_id) {
	    $query = $this->db->query("select announcement,announcement_type_id from announcement where student_id=".$student_id." limit 5");
		return $query->result();
      
  }
  
  public function fetch_student_assignment() {
      $query = $this->db->query("select assignment from assignment where group_id = 27");
	  return $query->result();
  }
 }
?>