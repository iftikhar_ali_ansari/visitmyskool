        <div class="row">
          <div class="span9">
            <h2 class="orange-line"><i class="icon-warning-sign icon-orange"></i>&nbsp;Announcements</h2>
			<div class="span9 alerts">
				<form>
				<fieldset>
				  <div class="announcements">
				  <div class="clearfix">
						<select class="large" name="mediumSelect" id="announcement_class_select">
						<option>Select Class</option>ss
						<optgroup label="Primary">
						<option>First - A Section</option>
						<option>Second - B Section</option>
						<option>Third - C Section</option>
						<option>Fourth - D Section</option>
						<option selected>Fifth - C Section</option>
						</optgroup>
						</select>
						<input type="checkbox" id="announcement_parent_check" name="optionsCheckboxes" value="parents"> 
						&nbsp;<span>Parents</span>
						
						
					</div>
				  <div class="clearfix"><textarea class="xxlarge" id="announcement" name="announcement" rows="3"></textarea></div>
				  <!-- clearfix -->
					<button type="button" class="btn success" id="submit_announcement"><i class='icon-bullhorn'></i>&nbsp;Announce</button>&nbsp;<button type="reset" class="btn">Cancel</button>
				  </div>
				</fieldset>
				</form>
			</div>
			
			<h2 class="yellow-line"><i class="icon-pencil icon-yellow"></i>&nbsp;Assignments</h2>
			<div class="span9 assignments">
				<form>
				<fieldset>
				  <div class="assignments">
				  <div class="clearfix">
						<select class="large" name="assignment_class" id="assignment_class">
						<option>Select Class</option>
						<optgroup label="Primary">
						<option>First - A Section</option>
						<option>Second - B Section</option>
						<option>Third - C Section</option>
						<option>Fourth - D Section</option>
						<option selected>Fifth - C Section</option>
						</optgroup>
						</select>
					</div>
					
				  <div class="clearfix"><textarea class="xxlarge" id="assignments" name="textarea2" rows="3"></textarea></div>
				  <!-- clearfix -->
					<button type="reset" id="assign_assignment" class="btn primary"><i class='icon-pencil'></i>&nbsp;Assign</button>&nbsp;<button type="reset" class="btn">Cancel</button>
				  </div>
				</fieldset>
				</form>
			</div>
			
          </div>
          <div class="span9">
		  <h2 class="blue-line"><i class="icon-calendar icon-blue"></i>&nbsp;Attendance</h2>
			<div class="span9">			
				<form>
				<fieldset>
				<div class="clearfix">
				    
					<select class="large" name="mediumSelect" id="mediumSelect">
					<option>Select Class</option>
					<optgroup label="Primary">
					<option>First - A Section</option>
					<option>Second - B Section</option>
					<option>Third - C Section</option>
					<option>Fourth - D Section</option>
					<option selected>Fifth - C Section</option>
					</optgroup>
					</select>
					<span style="padding-left:15px; cursor:pointer" id="prev_week"><i class="icon-chevron-sign-left icon-green"></i></span>
					<span style="padding-left:15px; cursor:pointer" id="next_week"><i class="icon-chevron-sign-right icon-green"></i></span>
					<div class="input-prepend" style="float: left;display: inline;padding-right: 30px;"><span class="add-on"><i class="icon-calendar"></i></span>
						<input type="text" class="span3" id="attendance_range" name="attendance_range" readonly="readonly">
					</div>
					
					
					<table class="bordered-table condensed-table attendance-table span9" id="attendance_table">
						<tr>
						    <th>#</th>
							<th>Name</th>
							<th colspan="2">03-Jun</th>
							<th colspan="2">04-Jun</th>
							<th colspan="2">05-Jun</th>
							<th colspan="2">06-Jun</th>
							<th colspan="2">07-Jun</th>
						</tr>
						<tr>
						    <td class="students_reg_num">1</td>
							<td class="students_names">Andy</td>
							<td style="background:#257432;"></td>
							<td style="background:#257432;border-right:2px solid white;"></td>
							<td style="background:#257432;"></td>
							<td style="background:#257432;border-right:2px solid white;"></td>
							<td style="background:#257432;"></td>
							<td style="background:#257432;border-right:2px solid white;"></td>
							<td style="background:#257432;"></td>
							<td style="background:#257432;border-right:2px solid white;"></td>
							<td style="background:#257432;"></td>
							<td style="background:#257432;border-right:2px solid white;"></td>
						</tr>
						<tr>
							<td class="students_reg_num">2</td>
							<td>Arnold</td>
							<td style="background:#257432;"></td>
							<td style="background:#257432;border-right:2px solid white;"></td>
							<td style="background:#257432;"></td>
							<td style="background:#257432;border-right:2px solid white;"></td>
							<td style="background:#257432;"></td>
							<td style="background:#257432;border-right:2px solid white;"></td>
							<td style="background:#257432;"></td>
							<td style="background:#257432;border-right:2px solid white;"></td>
							<td style="background:#257432;"></td>
							<td style="background:#257432;border-right:2px solid white;"></td>
						</tr>
						<tr>
						    <td class="students_reg_num">3</td>
							<td>Betty</td>
							<td style="background:#257432;"></td>
							<td style="background:#257432;border-right:2px solid white;"></td>
							<td style="background:#257432;"></td>
							<td style="background:#257432;border-right:2px solid white;"></td>
							<td style="background:#257432;"></td>
							<td style="background:#257432;border-right:2px solid white;"></td>
							<td style="background:#257432;"></td>
							<td style="background:#257432;border-right:2px solid white;"></td>
							<td style="background:#257432;"></td>
							<td style="background:#257432;border-right:2px solid white;"></td>
						</tr>
						<tr>
							<td class="students_reg_num">4</td>
							<td>Ben</td>
							<td style="background:#257432;"></td>
							<td style="background:#257432;border-right:2px solid white;"></td>
							<td style="background:#257432;"></td>
							<td style="background:#257432;border-right:2px solid white;"></td>
							<td style="background:#257432;"></td>
							<td style="background:#257432;border-right:2px solid white;"></td>
							<td style="background:#257432;"></td>
							<td style="background:#257432;border-right:2px solid white;"></td>
							<td style="background:#257432;"></td>
							<td style="background:#257432;border-right:2px solid white;"></td>
						</tr>
						<tr>
							<td class="students_reg_num">5</td>
							<td>Bernard</td>
							<td style="background:#257432;"></td>
							<td style="background:#257432;border-right:2px solid white;"></td>
							<td style="background:#257432;"></td>
							<td style="background:#257432;border-right:2px solid white;"></td>
							<td style="background:#257432;"></td>
							<td style="background:#257432;border-right:2px solid white;"></td>
							<td style="background:#257432;"></td>
							<td style="background:#257432;border-right:2px solid white;"></td>
							<td style="background:#257432;"></td>
							<td style="background:#257432;border-right:2px solid white;"></td>
						</tr>
					</table>
					<small>Single click to mark Absent. Double click to mark Present again</small>
				</div>
				
				</fieldset>
				<fieldset>
				<div class="pull-right">
					<button class="btn success" type="button" id="saveAttendance"><i class="icon-save"></i>&nbsp; Save</button>
				</div>
			</fieldset>
				</form>
			  </div>
          </div>
        </div>
     
   