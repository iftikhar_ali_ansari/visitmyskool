
        <div class="row">
          <div class="span9">
            <h2 class="orange-line"><i class="icon-warning-sign icon-orange"></i>&nbsp;Announcements</h2>
			<div class="span9 alerts" id="announcement">
			    
				<div class="alert-message error">
				<p><strong>Oh snap!</strong> This is very urgent <a href="#">notice</a>.</p>
				</div>
				<div class="alert-message info">
				<p><strong>Heads up!</strong> This is an alert that needs your attention, but it’s not <a href="#">a huge priority</a> just yet.</p>
				</div>
				<div class="alert-message success">
				<p><strong>Well done!</strong> You successfully <a href="#">read this</a> alert message.</p>
				</div>
				<div class="alert-message warning">
				<p><strong>Holy guacamole!</strong> Just a default notice <a href="#">looking too good</a>.</p>
				</div>
			</div>
			<h2 class="yellow-line"><i class="icon-pencil icon-yellow"></i>&nbsp;Assignments</h2>
			<div class="span9 assignments">
				<table class="bordered-table zebra-striped">
					<tbody id="assignment_table">
					  <tr>
						<td><i class="icon-check-empty">&nbsp;</i></td>
						<td>Science II: Complete first worksheet</td>
					  </tr>
					  <tr>
						<td><i class="icon-check-empty">&nbsp;</i></td>
						<td>English II: Memorize nouns and pronouns</td>
					  </tr>
					  <tr>
						<td><i class="icon-check-empty">&nbsp;</i></td>
						<td>Algebra: Exercise 5.11: 7,8,10</td>
					  </tr>
					  <tr>
						<td><i class="icon-check-sign">&nbsp;</i></td>
						<td>Biology: Get a roach specimen for disection</td>
					  </tr>
					  <tr>
						<td><i class="icon-check-sign">&nbsp;</i></td>
						<td>Chemistry: Draw and describe titration</td>
					  </tr>
					</tbody>
				  </table>
			</div>
			<h2 class="purple-line"><i class="icon-time icon-purple"></i>&nbsp;Timetable</h2>
			<div class="span9 timetable">
				<div class="tree">
					<ul>
						<li>
							<span><i class="icon-calendar"></i> Tomorrow</span>
							<ul>
								<li>
									<span class="badge badge-success"><i class="icon-time"></i> 8:00 AM to 9:00 AM</span>
									<ul>
										<li><a href=""><span><i class="icon-book"></i></span> &ndash; Science</a></li>
									</ul>
								</li>
								<li>
									<span class="badge badge-success"><i class="icon-time"></i> 9:00 AM to 10:00 AM</span>
									<ul>
										<li><a href=""><span><i class="icon-book"></i></span> &ndash; Maths</a></li>
									</ul>
								</li>
								<li>
									<span class="badge badge-success"><i class="icon-time"></i> 10:00 AM to 11:00 AM</span>
									<ul>
										<li><a href=""><span><i class="icon-book"></i></span> &ndash; Social</a></li>
									</ul>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
			<div class="span9 embed-widget">
				<h2 class="purple-line"><i class="icon-rocket icon-purple"></i>&nbsp;Science</h2>
				<script type="text/javascript" id="WolframAlphaScriptc767271e90f59c1ca07c9e94d0b5bfd2" src="http://www.wolframalpha.com/widget/widget.jsp?id=c767271e90f59c1ca07c9e94d0b5bfd2&theme=purple&output=iframe&width=385"></script>
			</div>
          </div>		  
          <div class="span9">
			<div class="span9 embed-widget">
				<h2 class="green-line"><i class="icon-eye-open icon-green"></i>&nbsp;Know something new today</h2>
				<script type="text/javascript" id="WolframAlphaScriptba48d3734a64119b505c755efce3540b" src="http://www.wolframalpha.com/widget/widget.jsp?id=ba48d3734a64119b505c755efce3540b&theme=green&output=iframe&width=385&height=300"></script>
				<!-- Place this code where you'd like the game to appear -->
				<div id="miniclip-game-embed" data-game-name="monkey-lander" data-theme="1" data-width="385" data-height="205" style="padding-top:10px;"></div>				
			</div>
			<div class="span9">
				<h2 class="blue-line"><i class="icon-smile icon-blue"></i>&nbsp;Friends</h2>
				<div class="friends">
				<div class="media">
				  <a class="pull-left" href="#">
					<img class="media-object" data-src="holder.js/32x32" alt="32x32" src="http://placehold.it/32x32/ff0000/ffffff/">
				  </a><small><b>Tom</b></small>
				  <div class="media-body">
					Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at.
				  </div>
				</div>
				
				<div class="media">
				  <a class="pull-left" href="#">
					<img class="media-object" data-src="holder.js/32x32" alt="32x32" src="<?php echo base_url(); ?>resources/img/profile_pic.jpg">
				  </a><small><b>John Mayer</b></small>
				  <div class="media-body">
					Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at.
				  </div>
				</div>
				
				<div class="media">
				  <a class="pull-left" href="#">
					<img class="media-object" data-src="holder.js/32x32" alt="32x32" src="http://placehold.it/32x32/990066/ffffff/">
				  </a><small><b>Dick</b></small>
				  <div class="media-body">
					Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at.
				  </div>
				</div>
				<div class="media">
				  <a class="pull-left" href="#">
					<img class="media-object" data-src="holder.js/32x32" alt="32x32" src="http://placehold.it/32x32/000000/ffffff/">
				  </a><small><b>Harry</b></small>
				  <div class="media-body">
					Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at.
				  </div>
				</div>
				
				<div class="media">
				  <a class="pull-left" href="#">
					<img class="media-object" data-src="<?php echo base_url(); ?>holder.js/32x32" alt="32x32" src="resources/img/profile_pic.jpg">
				  </a><small><b>John Mayer</b></small>
				  <div class="media-body">
					Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at.
				  </div>
				</div>
			  </div>
				<form>
				<fieldset>
				  <div class="actions friends-action">
				  <div class="clearfix"><textarea class="xxlarge" id="textarea2" name="textarea2" rows="3"></textarea></div>
				  <!-- clearfix -->
					<button class="btn success"><i class='icon-comment'></i>&nbsp;Comment</button>&nbsp;<button type="reset" class="btn">Cancel</button>
				  </div>
				</fieldset>
			  </form>
			</div>			
          </div>		  
        </div>