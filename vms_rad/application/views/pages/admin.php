<div class="content">
	<div class="row">
	  <div class="span20">		
		<div class="span12 pull-left" >		
			<div id="fee_entry" style="margin:0px 5px;">
				<h3 class="line green-line"><i class="icon-money icon-green"></i>&nbsp;Fees</h3>
				<span class="help-block pull-left">Receipt ID: <?php echo $next_reciept_no; ?> </span><span class="help-block pull-right">Dated: <?php echo get_today_date(); ?></span>
				<br><br>
				<div id="fee_entry_spreadsheet" class="handsontable"></div>
				<fieldset>
				<button class="btn success" id="save_fee_entry" onclick="print_table_content('payment_history')"><i class="icon-save">&nbsp;</i>&nbsp;Save</button>
				<button class="btn primary" id="save_print_fee_entry"><i class="icon-print" onclick="print_table_content('payment_history')">&nbsp;</i>&nbsp;Save &amp; Print</button>
				<button class="btn" id="cancel_fee_entry" ><i class="icon-remove"></i>&nbsp;Cancel</button>
				</fieldset>
			</div>
			
			<h3 class="line blue-line"><i class="icon-sort-by-attributes-alt icon-blue"></i>&nbsp;Payment history</h3>
			<table cellpadding="0" cellspacing="0" border="1" class="bordered-table condensed-table zebra-striped" id="payment_history" >
				<thead>
					<tr>
						<th>Receipt ID</th>
						<th>Paid date</th>
						<th>Description</th>						
						<th>Amount</th>
					</tr>
				</thead>
				<tbody>
				<tr>
					<td colspan="4" class="dataTables_empty">Loading data from server</td>
				</tr>
				</tbody>
			</table>			
		</div>
	
		<div class="span8 pull-right">
		<?php foreach($student_data as $student_profile_data) {	?>
			<h3 class="line purple-line"><i id="student_profile" class="icon-user icon-purple">&nbsp;</i>&nbsp;<span id="selected_student_name"><?php echo ucfirst($student_profile_data->first_name)." ".ucfirst($student_profile_data->last_name); ?></span></h3>
			<div class="profile">
				<div class="span2 pull-left" style="border-left:0px; min-height:160px;">
					<img src="<?php echo base_url(); ?>resources/img/profile_pic.jpg" class="img-polaroid tint" style="height:75px;">
				</div>
				<div class="span6 pull-right" style="border-left:0px; min-height:160px;">
					<h4><b>Reg. no:</b> &nbsp; <span id="selected_student_reg_no"><?php echo $student_profile_data->student_id; ?></span></h4>
					<h4><b>Class:</b> &nbsp; VI C</h4>
					<h4><b>Father:</b> &nbsp; <span id="selected_student_father_name"><?php echo $student_profile_data->father_name; ?></span></h4>
					<h4><b>Mother:</b> &nbsp; <span id="selected_student_mother_name"><?php echo $student_profile_data->mother_name; ?></span></h4>
				</div>
			</div>
		<?php }	?>
		</div>
		
	<div class="span8 pull-right">
		<h3 class="line blue-line"><i class="icon-inr icon-blue"></i>&nbsp;Balances</h3>
			<table class="bordered-table condensed-table zebra-striped" id="balances">
					<tbody><tr>
						<th>Description</th>
						<th>Due date</th>
						<th>Amount</th>
						<th></th>
					</tr>
					<?php foreach($student_balance_data as $balance_data) { ?>
					<tr>
						<td><?php echo $balance_data->fee_description ;?></td>
						<td><?php echo $balance_data->due_date; ?></td>
						<td class="numerals"><?php echo $balance_data->fee_amount; ?></td>
						<?php
							$icon_chosen = "icon-green";
							if(is_duedate_crossed($balance_data->due_date)) {
								$icon_chosen = "icon-orange";
							}
						?>
						<td><i class="icon-circle <?php echo $icon_chosen;?>" ></i></td>
					</tr>
					<?php } ?>
					<tr>
						<td colspan="2" style="text-align:right;">Grand Total</td>
						<td class="numerals"><?php echo $student_balance_total;?></td>
						<td></td>
					</tr>
				</tbody>
			</table>				
		</div>
		</div>
	</div>
</div>