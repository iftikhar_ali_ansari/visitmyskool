<form class="form-search">
		<div class="clearfix">
			<table class="bordered-table zebra-striped">			
			<thead>
			  <tr>
				<th colspan="4">
					<input type="text" class="span14 search-query" value="An">
					<button type="submit" class="btn small success" style="padding-top:3px;">Search</button>
				</th>
			  </tr>
			</thead>
			<tbody>
			<tr>
				<td colspan="4">
				  <b>Primary School</b>
				</td>
			  </tr>
			  <tr>
				<td class="span1">
				<b>75</b></td>
				<td class="span2"><b>Andy</b></td>				
				<td><b>VI C</b></td>
				<td><b><i class="float-right icon-chevron-right">&nbsp;</i></b></td>
			  </tr>
			  <tr>
				<td>20</td>
				<td>Andy A</td>
				<td>VI C</td>
				<td></td>
			  </tr>
			  <tr>
				<td>3</td>
				<td>Andy X</td>
				<td>VI C</td>
				<td></td>
			  </tr>
			  <tr>
				<td>14</td>
				<td>Anthony B</td>
				<td>VI C</td>
				<td></td>
			  </tr>
			  <tr>
				<td>25</td>
				<td>Antony C</td>
				<td>VI C</td>				
				<td></td>
			  </tr>
			  <tr>
				<td>6</td>
				<td>Anoop</td>
				<td>VI C</td>
				<td></td>
			  </tr>
			  <tr>
				<td>7</td>
				<td>Anu</td>
				<td>VI C</td>
				<td></td>
			  </tr>
			  <tr>
				<td>18</td>
				<td>Anuj</td>
				<td>VI C</td>
				<td></td>
			  </tr>
			  <tr>
				<td>9</td>
				<td>Anusha</td>
				<td>VI C</td>
				<td></td>
			  </tr>
			  
			  <tr>
				<td colspan="4">
				  <b>High School</b>
				</td>
			  </tr>
			  <tr>
				<td>
				  24
				</td>
				<td>
				  Anoop
				</td>
				<td>IX D</td>
				<td></td>
			  </tr>
			  <tr>
				<td>36</td>
				<td>Anup</td>
				<td>IX D</td>
				<td></td>
			  </tr>
			  <tr>
				<td>17</td>
				<td>Anna Kolslovakia</td>
				<td>IX D</td>
				<td></td>
			  </tr>
			</tbody>
		  </table>
		</div>	
		</form>