<form class="form-search">
		<div class="clearfix">
			<table class="bordered-table zebra-striped pull-left" id="students">			
			<thead>
			  <tr>
				<tr>		
					<th width="1%"><button class="btn small" id="add_new_user"class="modal-link" href="add-new-user-modal" data-controls-modal="add-new-user-modal" data-backdrop="true" data-keyboard="true"><i class="icon-user"></i></button></th>
					<th width="1%"><input type="text" class="span15" id="search-query"></th>
				</tr>
			  </tr>
			</thead>
			<tbody>
			<tr>
				<td colspan="2" class="dataTables_empty">Loading data from server</td>
			</tr>
			</tbody>
	  </table>
	</div>
</form>


<div id="add-new-user-modal" class="modal hide fade" style="display: none;width:655px;">
	<div class="modal-header">
	  <a href="#" class="close">&times;</a>
	  <h3><i class="icon-user"></i>&nbsp;New Admission Details</h3>
	</div>	
	<div class="modal-body" style="height:450px;overflow:auto;">
		<div class="span12">
		<form action='admin/create' method='post'>
        <fieldset>
		<div class="row">
		<div class="span12" style="width: 100%;margin-left: 5px;">
          <div class="span6" style="margin-left: -10px;">
		   <div class="clearfix">
            <label for="First_Name">First Name</label>
            <div class="input">
              <input class="large " id="First_Name" name="First_Name" size="30" type="text">									  
            </div>			
          </div><!-- /clearfix -->		  
		   <div class="clearfix">
            <label for="Last_Name">Last Name</label>
            <div class="input">
              <input class="large" id="Last_Name" name="Last_Name" size="30" type="text">
            </div>
          </div><!-- /clearfix -->
		   <div class="clearfix">
            <label for="Student_Email_Id">Student's Email Id</label>
            <div class="input">
              <input class="large" id="Student_Email_Id" name="Student_Email_Id" size="30" type="text">
            </div>
          </div><!-- /clearfix -->
		  <div class="clearfix">
            <label for="Father_Name">Father Name</label>
            <div class="input">
              <input class="large" id="Father_Name" name="Father_Name" size="30" type="text">
            </div>
          </div><!-- /clearfix -->
		  <div class="clearfix">
            <label for="Father_Email_Id">Father's Email Id</label>
            <div class="input">
              <input class="large" id="Father_Email_Id" name="Father_Email_Id" size="30" type="text">
            </div>
          </div><!-- /clearfix -->
		   <div class="clearfix">
            <label for="Father_Mobile_No">Father's Mobile No</label>
            <div class="input">
              <input class="large" id="Father_Mobile_No" name="Father_Mobile_No" size="30" type="text">
            </div>
          </div><!-- /clearfix --> 
		  <div class="clearfix">
            <label for="Address">Address</label>
            <div class="input">
              <textarea class="xxlarge" name="Address" id="Address" rows="5" ></textarea>
            </div>
          </div><!-- /clearfix -->  
		<div class="clearfix">
            <label for="Locality">Locality</label>
            <div class="input">
              <input class="large" id="Locality" name="Locality" size="30" type="text">
            </div>
          </div><!-- /clearfix -->	
		   <div class="clearfix">
            <label for="PIN_Code">PIN Code</label>
            <div class="input">
              <input class="medium" id="PIN_Code" name="PIN_Code" size="30" type="text">
            </div>
          </div><!-- /clearfix -->
		  </div>
		  <div class="span6"style="float: right;margin-top: -396px;">
		   <div class="clearfix">
            <label for="Middle_Name">Middle Name</label>
            <div class="input">
              <input class="large " id="Middle_Name" name="Middle_Name" size="30" type="text">									  
            </div>			
          </div><!-- /clearfix -->
		   <div class="clearfix" style="margin-left: 82px;width: 245px;margin-top: -6px;">
			<label class="select-inline" style="margin-left: -3px;">Class<select class="small" name="Class_" id="Class_" style="margin-left: 20px;">
                <option>I</option>
                <option>II</option>
                <option>III</option>
                <option>IV</option>
                <option>V</option>
				<option>VI</option>
                <option>VII</option>
                <option>VIII</option>
                <option>IX</option>
                <option>X</option>
              </select>
			</label>
			<label class="select-inline" style="margin-left: -15px;">Section<select class="small" name="Section" id="Section" style="margin-left: 4px;">
                <option>A</option>
                <option>B</option>
                <option>C</option>
                <option>D</option>
                <option>E</option>
              </select>
			</label>		  		  		           
          </div><!-- /clearfix -->
		  <div class="clearfix">
            <label for="DOB">DOB</label>
            <div class="input">
              <input class="large" id="DOB" name="DOB" size="30" type="text">
            </div>
          </div><!-- /clearfix -->
		   <div class="clearfix">
            <label for="Mother_Name">Mother Name</label>
            <div class="input">
              <input class="large" id="Mother_Name" name="Mother_Name" size="30" type="text">
            </div>
          </div><!-- /clearfix -->
		  <div class="clearfix">
            <label for="Mother_Email_Id">Mother's Email Id</label>
            <div class="input">
              <input class="large" id="Mother_Email_Id" name="Mother_Email_Id" size="30" type="text">
            </div>
          </div><!-- /clearfix -->
		  <div class="clearfix">
            <label for="Mother_Mobile_No">Mother's Mobile No</label>
            <div class="input">
              <input class="large" id="Mother_Mobile_No" name="Mother_Mobile_No" size="30" type="text">
            </div>
          </div><!-- /clearfix -->
		   <div class="clearfix" style="margin-top:108px;">
            <label for="City">City</label>
            <div class="input">
              <input class="large" id="City" name="City" size="30" type="text">
            </div>
          </div><!-- /clearfix -->
		  <div class="clearfix">
            <label for="xlInput">Country</label>
            <div class="input">
			<select id="Country" class="large" name="Country">
				<option value="">Country...</option>
				<option value="AF">Afghanistan</option>
				<option value="AL">Albania</option>
				<option value="DZ">Algeria</option>
				<option value="AS">American Samoa</option>
				<option value="AD">Andorra</option>
				<option value="AG">Angola</option>
				<option value="AI">Anguilla</option>
				<option value="AG">Antigua &amp; Barbuda</option>
				<option value="AR">Argentina</option>
				<option value="AA">Armenia</option>
				<option value="AW">Aruba</option>
				<option value="AU">Australia</option>
				<option value="AT">Austria</option>
				<option value="AZ">Azerbaijan</option>
				<option value="BS">Bahamas</option>
				<option value="BH">Bahrain</option>
				<option value="BD">Bangladesh</option>
				<option value="BB">Barbados</option>
				<option value="BY">Belarus</option>
				<option value="BE">Belgium</option>
				<option value="BZ">Belize</option>
				<option value="BJ">Benin</option>
				<option value="BM">Bermuda</option>
				<option value="BT">Bhutan</option>
				<option value="BO">Bolivia</option>
				<option value="BL">Bonaire</option>
				<option value="BA">Bosnia &amp; Herzegovina</option>
				<option value="BW">Botswana</option>
				<option value="BR">Brazil</option>
				<option value="BC">British Indian Ocean Ter</option>
				<option value="BN">Brunei</option>
				<option value="BG">Bulgaria</option>
				<option value="BF">Burkina Faso</option>
				<option value="BI">Burundi</option>
				<option value="KH">Cambodia</option>
				<option value="CM">Cameroon</option>
				<option value="CA">Canada</option>
				<option value="IC">Canary Islands</option>
				<option value="CV">Cape Verde</option>
				<option value="KY">Cayman Islands</option>
				<option value="CF">Central African Republic</option>
				<option value="TD">Chad</option>
				<option value="CD">Channel Islands</option>
				<option value="CL">Chile</option>
				<option value="CN">China</option>
				<option value="CI">Christmas Island</option>
				<option value="CS">Cocos Island</option>
				<option value="CO">Colombia</option>
				<option value="CC">Comoros</option>
				<option value="CG">Congo</option>
				<option value="CK">Cook Islands</option>
				<option value="CR">Costa Rica</option>
				<option value="CT">Cote D'Ivoire</option>
				<option value="HR">Croatia</option>
				<option value="CU">Cuba</option>
				<option value="CB">Curacao</option>
				<option value="CY">Cyprus</option>
				<option value="CZ">Czech Republic</option>
				<option value="DK">Denmark</option>
				<option value="DJ">Djibouti</option>
				<option value="DM">Dominica</option>
				<option value="DO">Dominican Republic</option>
				<option value="TM">East Timor</option>
				<option value="EC">Ecuador</option>
				<option value="EG">Egypt</option>
				<option value="SV">El Salvador</option>
				<option value="GQ">Equatorial Guinea</option>
				<option value="ER">Eritrea</option>
				<option value="EE">Estonia</option>
				<option value="ET">Ethiopia</option>
				<option value="FA">Falkland Islands</option>
				<option value="FO">Faroe Islands</option>
				<option value="FJ">Fiji</option>
				<option value="FI">Finland</option>
				<option value="FR">France</option>
				<option value="GF">French Guiana</option>
				<option value="PF">French Polynesia</option>
				<option value="FS">French Southern Ter</option>
				<option value="GA">Gabon</option>
				<option value="GM">Gambia</option>
				<option value="GE">Georgia</option>
				<option value="DE">Germany</option>
				<option value="GH">Ghana</option>
				<option value="GI">Gibraltar</option>
				<option value="GB">Great Britain</option>
				<option value="GR">Greece</option>
				<option value="GL">Greenland</option>
				<option value="GD">Grenada</option>
				<option value="GP">Guadeloupe</option>
				<option value="GU">Guam</option>
				<option value="GT">Guatemala</option>
				<option value="GN">Guinea</option>
				<option value="GY">Guyana</option>
				<option value="HT">Haiti</option>
				<option value="HW">Hawaii</option>
				<option value="HN">Honduras</option>
				<option value="HK">Hong Kong</option>
				<option value="HU">Hungary</option>
				<option value="IS">Iceland</option>
				<option value="IN">India</option>
				<option value="ID">Indonesia</option>
				<option value="IA">Iran</option>
				<option value="IQ">Iraq</option>
				<option value="IR">Ireland</option>
				<option value="IM">Isle of Man</option>
				<option value="IL">Israel</option>
				<option value="IT">Italy</option>
				<option value="JM">Jamaica</option>
				<option value="JP">Japan</option>
				<option value="JO">Jordan</option>
				<option value="KZ">Kazakhstan</option>
				<option value="KE">Kenya</option>
				<option value="KI">Kiribati</option>
				<option value="NK">Korea North</option>
				<option value="KS">Korea South</option>
				<option value="KW">Kuwait</option>
				<option value="KG">Kyrgyzstan</option>
				<option value="LA">Laos</option>
				<option value="LV">Latvia</option>
				<option value="LB">Lebanon</option>
				<option value="LS">Lesotho</option>
				<option value="LR">Liberia</option>
				<option value="LY">Libya</option>
				<option value="LI">Liechtenstein</option>
				<option value="LT">Lithuania</option>
				<option value="LU">Luxembourg</option>
				<option value="MO">Macau</option>
				<option value="MK">Macedonia</option>
				<option value="MG">Madagascar</option>
				<option value="MY">Malaysia</option>
				<option value="MW">Malawi</option>
				<option value="MV">Maldives</option>
				<option value="ML">Mali</option>
				<option value="MT">Malta</option>
				<option value="MH">Marshall Islands</option>
				<option value="MQ">Martinique</option>
				<option value="MR">Mauritania</option>
				<option value="MU">Mauritius</option>
				<option value="ME">Mayotte</option>
				<option value="MX">Mexico</option>
				<option value="MI">Midway Islands</option>
				<option value="MD">Moldova</option>
				<option value="MC">Monaco</option>
				<option value="MN">Mongolia</option>
				<option value="MS">Montserrat</option>
				<option value="MA">Morocco</option>
				<option value="MZ">Mozambique</option>
				<option value="MM">Myanmar</option>
				<option value="NA">Nambia</option>
				<option value="NU">Nauru</option>
				<option value="NP">Nepal</option>
				<option value="AN">Netherland Antilles</option>
				<option value="NL">Netherlands (Holland, Europe)</option>
				<option value="NV">Nevis</option>
				<option value="NC">New Caledonia</option>
				<option value="NZ">New Zealand</option>
				<option value="NI">Nicaragua</option>
				<option value="NE">Niger</option>
				<option value="NG">Nigeria</option>
				<option value="NW">Niue</option>
				<option value="NF">Norfolk Island</option>
				<option value="NO">Norway</option>
				<option value="OM">Oman</option>
				<option value="PK">Pakistan</option>
				<option value="PW">Palau Island</option>
				<option value="PS">Palestine</option>
				<option value="PA">Panama</option>
				<option value="PG">Papua New Guinea</option>
				<option value="PY">Paraguay</option>
				<option value="PE">Peru</option>
				<option value="PH">Philippines</option>
				<option value="PO">Pitcairn Island</option>
				<option value="PL">Poland</option>
				<option value="PT">Portugal</option>
				<option value="PR">Puerto Rico</option>
				<option value="QA">Qatar</option>
				<option value="ME">Republic of Montenegro</option>
				<option value="RS">Republic of Serbia</option>
				<option value="RE">Reunion</option>
				<option value="RO">Romania</option>
				<option value="RU">Russia</option>
				<option value="RW">Rwanda</option>
				<option value="NT">St Barthelemy</option>
				<option value="EU">St Eustatius</option>
				<option value="HE">St Helena</option>
				<option value="KN">St Kitts-Nevis</option>
				<option value="LC">St Lucia</option>
				<option value="MB">St Maarten</option>
				<option value="PM">St Pierre &amp; Miquelon</option>
				<option value="VC">St Vincent &amp; Grenadines</option>
				<option value="SP">Saipan</option>
				<option value="SO">Samoa</option>
				<option value="AS">Samoa American</option>
				<option value="SM">San Marino</option>
				<option value="ST">Sao Tome &amp; Principe</option>
				<option value="SA">Saudi Arabia</option>
				<option value="SN">Senegal</option>
				<option value="RS">Serbia</option>
				<option value="SC">Seychelles</option>
				<option value="SL">Sierra Leone</option>
				<option value="SG">Singapore</option>
				<option value="SK">Slovakia</option>
				<option value="SI">Slovenia</option>
				<option value="SB">Solomon Islands</option>
				<option value="OI">Somalia</option>
				<option value="ZA">South Africa</option>
				<option value="ES">Spain</option>
				<option value="LK">Sri Lanka</option>
				<option value="SD">Sudan</option>
				<option value="SR">Suriname</option>
				<option value="SZ">Swaziland</option>
				<option value="SE">Sweden</option>
				<option value="CH">Switzerland</option>
				<option value="SY">Syria</option>
				<option value="TA">Tahiti</option>
				<option value="TW">Taiwan</option>
				<option value="TJ">Tajikistan</option>
				<option value="TZ">Tanzania</option>
				<option value="TH">Thailand</option>
				<option value="TG">Togo</option>
				<option value="TK">Tokelau</option>
				<option value="TO">Tonga</option>
				<option value="TT">Trinidad &amp; Tobago</option>
				<option value="TN">Tunisia</option>
				<option value="TR">Turkey</option>
				<option value="TU">Turkmenistan</option>
				<option value="TC">Turks &amp; Caicos Is</option>
				<option value="TV">Tuvalu</option>
				<option value="UG">Uganda</option>
				<option value="UA">Ukraine</option>
				<option value="AE">United Arab Emirates</option>
				<option value="GB">United Kingdom</option>
				<option value="US">United States of America</option>
				<option value="UY">Uruguay</option>
				<option value="UZ">Uzbekistan</option>
				<option value="VU">Vanuatu</option>
				<option value="VS">Vatican City State</option>
				<option value="VE">Venezuela</option>
				<option value="VN">Vietnam</option>
				<option value="VB">Virgin Islands (Brit)</option>
				<option value="VA">Virgin Islands (USA)</option>
				<option value="WK">Wake Island</option>
				<option value="WF">Wallis &amp; Futana Is</option>
				<option value="YE">Yemen</option>
				<option value="ZR">Zaire</option>
				<option value="ZM">Zambia</option>
				<option value="ZW">Zimbabwe</option>
				</select>              
            </div>
          </div><!-- /clearfix -->
		  </div>
		  </div>
         </div>		</div>
</div>
		 <div class="modal-footer">  
  <input type="submit" class="btn success" value="Save changes" id='submit'>&nbsp;
	</div> 	                     		  	  		  				  		  		  		 	  		 		                                	  		 	  		 				  
        </fieldset>			    
      </form>			


	</div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>resources/js/custom/google-plus.js"></script> 


<script type="text/javascript"> 
$('#submit').click(function () {
	var First_Name = $('#First_Name').val();
		if(!First_Name)
		{
			alert('First Name Has to be entered');
			return false;
		}

	var Last_Name = $('#Last_Name').val();
		if(!Last_Name)
		{
			alert('Last Name Has to be entered');
			return false;
		}

var Student_Email_Id = $('#Student_Email_Id').val();
		if(!Student_Email_Id)
		{
			alert("Student's Email Id Has to be entered");
			return false;
		}

var Father_Name = $('#Father_Name').val();
		if(!Father_Name)
		{
			alert('Father Name Has to be entered');
			return false;
		}

var Father_Email_Id = $('#Father_Email_Id').val();
		if(!Father_Email_Id)
		{
			alert("Father's Email Id Has to be entered");
			return false;
		}

var Father_Mobile_No = $('#Father_Mobile_No').val();
		if(!Father_Mobile_No)
		{
			alert("Father's Mobile No Has to be entered");
			return false;
		}

var Address = $('#Address').val();
		if(!Address)
		{
			alert('Address Has to be entered');
			return false;
		}

var Locality = $('#Locality').val();
		if(!Locality)
		{
			alert('Locality Has to be entered');
			return false;
		}

var PIN_Code = $('#PIN_Code').val();
		if(!PIN_Code)
		{
			alert('PIN Code Has to be entered');
			return false;
		}

var Middle_Name = $('#Middle_Name').val();
		if(!Middle_Name)
		{
			alert('Middle Name Has to be entered');
			return false;
		}

var DOB = $('#DOB').val();
		if(!DOB)
		{
			alert('DOB Has to be entered');
			return false;
		}
var Mother_Name = $('#Mother_Name').val();
		if(!Mother_Name)
		{
			alert('Mother Name Has to be entered');
			return false;
		}
var Mother_Email_Id = $('#Mother_Email_Id').val();
		if(!Mother_Email_Id)
		{
			alert("Mother's Email Id Has to be entered");
			return false;
		}
var Mother_Mobile_No = $('#Mother_Mobile_No').val();
		if(!Mother_Mobile_No)
		{
			alert("Mother's Mobile No Has to be entered");
			return false;
		}
var City = $('#City').val();
		if(!City)
		{
			alert('City Has to be entered');
			return false;
		}
var Country = $('#Country').val();
		if(Country == "")
		{
			alert('Choose the Country');
			return false;
		}
 
		var form_data = {
	First_Name: $('#First_Name').val(),
	Last_Name: $('#Last_Name').val(),
	Student_Email_Id: $('#Student_Email_Id').val(),
	Father_Name: $('#Father_Name').val(),
	Father_Email_Id: $('#Father_Email_Id').val(),
	Father_Mobile_No: $('#Father_Mobile_No').val(),
	Address: $('#Address').val(),
	Locality: $('#Locality').val(),
	PIN_Code: $('#PIN_Code').val(),
	Middle_Name: $('#Middle_Name').val(),
	Class_: $('#Class_').val(),
	Section: $('#Section').val(),
	DOB: $('#DOB').val(),
	Mother_Name: $('#Mother_Name').val(),
	Mother_Email_Id: $('#Mother_Email_Id').val(),
	Mother_Mobile_No: $('#Mother_Mobile_No').val(),
	City: $('#City').val(),
	Country: $('#Country').val()
//	ajax: '1'
					};

 $.ajax({
		url: "<?php echo site_url('admin/create'); ?>",
		type: 'POST',
		data: form_data,
		success: function(msg) {
		alert(msg);
            
			
 }  
});  
return false;  


});
</script>
	


