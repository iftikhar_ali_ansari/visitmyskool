<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Visitmyskool.com | Online school management simplified</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le styles -->
    <link href="<?php echo base_url();?>resources/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url();?>resources/css/bootstrap-custom.css" rel="stylesheet">
     <link href="<?php echo base_url();?>resources/css/login.css" rel="stylesheet">
    
    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="images/favicon.ico">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">
    
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js" ></script>
  <script src="<?php echo base_url();?>resources/js/custom/google-plus.js" ></script>
  </head>
  <body class="admin-body">
    <div class="topbar">
      <div class="fill">
        <div class="container">
          <a class="brand" href="#">Visitmyskool.com</a>
         </div>
      </div>
    </div>
    <div class="container">
      <!-- Main hero unit for a primary marketing message or call to action -->
      <div class="hero-unit">
      <h1><span class="black-text">Visit</span><span class="orange-red-text">myskool</span><span class="black-text">.com</span></h1>
      <h2>Online School Management Simplified</h2>
        <p><div id="gConnect" class="pull-right">
	    <button class="g-signin"
	        data-scope="https://www.googleapis.com/auth/plus.login"
	        data-requestvisibleactions="http://schemas.google.com/AddActivity"
	        data-clientId="492829686551.apps.googleusercontent.com"
	        data-accesstype="offline"
	        data-callback="onSignInCallback"
	        data-theme="dark"
	        data-cookiepolicy="single_host_origin">
	    </button></p>
  		</div>
      </div>
	<footer>
	<p>&copy; Visitmyskool 2013 | All rights reserved | Built by <a href="http://www.preranaconsulting.com" target="_blank">Preana Consulting</a></p>
	</footer>
	</div>
</body>
</html>
