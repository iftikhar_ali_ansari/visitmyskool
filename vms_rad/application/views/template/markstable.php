<script src="http://warpech.github.io/jquery-handsontable/lib/jquery.min.js"></script>
	<script src="http://warpech.github.io/jquery-handsontable/dist/jquery.handsontable.full.js"></script>
	<script>
		$(document).ready(function() {
		
			var marks_validator_regexp = new RegExp("\\d\\d");
			var colCount = 0;
			$("#subjectSelect").change(function () {
				var str = "";
				$("#subjectSelect option:selected").each(function () {
					str += $(this).val();	
					colCount = str;
					colCount++;
				});
				
				if(str != 0){
					$("#datatable tr > td, tr > th").show();
					$("#datatable tr > td:nth-child("+colCount+"), tr > th:nth-child("+colCount+")").nextAll().hide();
					var columnTotalCount = $("#datatable tr th").length;
					$("#datatable tr > td:nth-child("+columnTotalCount+"), tr > th:nth-child("+columnTotalCount+")").show();
				} else {
					$("#datatable tr > td:nth-child("+colCount+"), tr > th:nth-child("+colCount+")").prevAll().show();
					$("#datatable tr > td:nth-child("+colCount+"), tr > th:nth-child("+colCount+")").nextAll().show();
				}
			})
			.trigger('change');
			
			var greyRenderer = function (instance, td, row, col, prop, value, cellProperties) {
			Handsontable.TextCell.renderer.apply(this, arguments);
				$(td).css({background: '#EEE'});
				$(td).css({color: '#000'});
				$(td).css({style: 'normal'});
			};
		
			function getCarData() {
			$name='';
			$.ajax({
	            url: "<?php echo base_url(); ?>index.php/admin/fetch_student_names",
		        type: "get",
		        datatype: "html",
		        async:false,
		        success: function(data) {
		            $name = $.parseJSON(data);
		        }
	        });
	        return $name;
			}
			
	
			var lastChange = null;
			var $container = $("#datatable").handsontable({
				data:getCarData(),
				minSpareRows: 0,
				stretchH: 'all',
				contextMenu: false,
				colHeaders: ["Name", "English","Hindi", "Kannada","Maths","Science","Social","Computers", "EVS", "General", "Remarks"],
				columns: [
				{
				  data: "name",
				  readOnly: true
				},
				{
				  data: "english",
				  validator: marks_validator_regexp, 
				  allowInvalid: false
				},
				{
				  data: "hindi",
				  validator: marks_validator_regexp, 
				  allowInvalid: false
				},
				{
				  data: "kannada",
				  validator: marks_validator_regexp, 
				  allowInvalid: false
				},
				{
				  data: "maths",
				  validator: marks_validator_regexp, 
				  allowInvalid: false
				},
				{
				  data: "science",
				  validator: marks_validator_regexp, 
				  allowInvalid: false
				},
				{
				  data: "social",
				  validator: marks_validator_regexp, 
				  allowInvalid: false
				},
				{
				  data: "computers",
				  validator: marks_validator_regexp, 
				  allowInvalid: false
				},
				{
				  data: "evs",
				  validator: marks_validator_regexp, 
				  allowInvalid: false
				},
				{
				  data: "general",
				  validator: marks_validator_regexp, 
				  allowInvalid: false
				},
				{
				  data: "remarks"
				}
				],
				manualColumnResize: false,
				enterBeginsEditing:false,
				strict:true,
				autoWrapCol: true,
				useFormula: true,
				currentRowClassName: 'currentRow',
				currentColClassName: 'currentCol',
				autoWrapRow: true,
				cells: function (row, col, prop) {
					if (col == 0) {
					  return {type: {renderer: greyRenderer}};
					}			
				},
				afterChange: function(changes, source){
					return false;
				},
				beforeKeyDown: function(e){
					var HOT = $container.handsontable('getInstance'), selection = HOT.getSelected();
					if (e.keyCode === 8 || e.keyCode === 46) { //BACKSPACE or DELETE
					  e.stopImmediatePropagation();
					  e.preventDefault();
					  $("#datatables").handsontable('setDataAtCell',selection[0],selection[1], ' ');
					}
					if ( (e.keyCode === 13) || (e.keyCode === 9)){
						if($('#internalsCheck').is(':checked')){						
							var actualMarks = $("#datatable").handsontable('getDataAtCell',selection[0], selection[1]);
							var studentName = $("#datatable").handsontable('getDataAtCell',selection[0], 0);
							var subjectName = $("#datatable").handsontable('getColHeader',selection[1], selection[1]);
							if(typeof(actualMarks) != "undefined"){
								if( (subjectName != "Name") && (subjectName != "Remarks") ){
									if(actualMarks.search(" + ") === -1){
										var internalMarks = prompt("Internal Marks of "+studentName+" for "+subjectName);
										if(internalMarks){
											if (internalMarks!=null && internalMarks!="")
											{
												if(actualMarks == "") {
													actualMarks = 0;
												}
												$("#datatable").handsontable('setDataAtCell',selection[0],selection[1], actualMarks);
												if(isNumber(internalMarks)) {
													var studentTotalMarks = actualMarks + " + " + internalMarks;
													$("#datatable").handsontable('setDataAtCell',selection[0],selection[1], studentTotalMarks);													
												} else {
													alert("Internal marks can only be numbers");
													var studentTotalMarks = actualMarks + " + 0";
													$("#datatable").handsontable('setDataAtCell',selection[0],selection[1], studentTotalMarks);	
												}
											}
										} else {
											var studentTotalMarks = actualMarks + " + 0";
											$("#datatable").handsontable('setDataAtCell',selection[0],selection[1], studentTotalMarks);	
										}										
									}
								}
							}
						}
					}				
				}
			});
			
			function isNumber(n) {
			  return !isNaN(parseFloat(n)) && isFinite(n);
			}
		});
  </script>
  <style>
	.handsontable .currentRow, .handsontable .currentCol {
		background-color: #E7E8EF;
	}
  </style>