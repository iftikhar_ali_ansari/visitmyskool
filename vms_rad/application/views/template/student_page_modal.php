	<div id="attendance-modal" class="modal hide fade" style="display: none;">
	<div class="modal-header">
	  <a href="#" class="close" data-dismiss="modal">&times;</a>
	  <h3><i class="icon-calendar"></i>&nbsp;Attendance</h3>
	</div>	
	<div class="modal-body">
	<span class="pull-right">
	  <span class="label success">Present</span>
	  <span class="label important">Absent</span>
	  <span class="label warning">Holidays</span>
	  </span>
	<div class="pagination">
	    <a href="#" id="prev_year"><i class="icon-chevron-sign-left"></i></a>
	<ul class="nav nav-pills" id="year">
		<li class="active"><a href="#" class="year">2013-14</a></li>
		<li><a href="#" class="year">2014-15</a></li>
		<li><a href="#" class="year">2015-16</a></li>
		<li><a href="#" class="year">2016-17</a></li>
		<li><a href="#" class="year">2017-18</a></li>
	</ul>
	    <a href="#" id="next_year"><i class="icon-chevron-sign-right"></i></a>
	</div>
	<br>
      <ul class="nav nav-pills" id="month">				  		
			<li class="active"><a href="#" class="month" id="m1">June</a></li>
			<li><a href="#"  class="month" id="m2">July</a></li>
			<li><a href="#"  class="month" id="m3">Aug</a></li>
			<li><a href="#"  class="month" id="m4">Sep</a></li>
			<li><a href="#"  class="month" id="m5">Oct</a></li>
			<li><a href="#"  class="month" id="m6">Nov</a></li>
			<li><a href="#"  class="month" id="m7">Dec</a></li>
			<li><a href="#"  class="month" id="m8">Jan</a></li>
			<li><a href="#"  class="month" id="m9">Feb</a></li>
			<li><a href="#"  class="month" id="m10">March</a></li>
			<li><a href="#"  class="month" id="m11">April</a></li>
			<li><a href="#"  class="month" id="m12">May</a></li>
		</ul>
	  <div id="chart" style="display: block;">
	  <table class="table table-striped table-bordered table-condensed calendar" >
		<thead class="themed">  
		  <tr id="first_15_date">
            <!-- here the first 15 date will display -->
			
			</tr>
		</thead>
		<tbody>
		  <tr id="first_15_morning_hour">
		    <!-- morning hour attendance -->
		  </tr>
		  
		  <tr id="first_15_evening_hour">
		  <!-- evening hour attendance -->
		  </tr> 
		</tbody>
		</table>
		<table class="table table-striped table-bordered table-condensed calendar">
		<thead class="themed">  
		  <tr id="next_remaining_date">
           <!-- here the remaining date will display -->
			</tr>
		</thead>
		<tbody>
		  <tr id="next_morning_hour">
		     <!-- next morning hour attendance -->
		  </tr>
		  
		  <tr id="next_evening_hour">
		 
		  <!-- next evening hour attendance -->
		  </tr> 
		</tbody>
		</table>
		</div>
	</div>
		<div class="modal-footer">
		<div class="pagination pull-right">
		  <ul>
			<li class="prev"><a href="#" id="prev"><i class="icon-chevron-sign-left"></i>&nbsp; Prev</a></li>
			<li class="next"><a href="#" id="next">Next &nbsp;<i class="icon-chevron-sign-right"></i></a></li>
		  </ul>
		</div>
	</div>
	</div>
		<div id="profile-modal" class="modal hide fade" style="display: none;">
	<div class="modal-header">
	  <a href="#" class="close" data-dismiss="modal">&times;</a>
	  <h3><i class="icon-user"></i>&nbsp;Personal Details</h3>
	</div>	
	<div class="modal-body">
	    <div class="container">
		<div class="row">
		<div class="span18">
			<div class="span4 pull-left" style="border-left:0px; min-height:160px;">
				<h4>Reg. no: &nbsp; <?php echo $reg_number; ?></h4>
				<h4>Class: &nbsp;  <?php echo $class; ?></h4>
				<h4>Father: &nbsp; <?php echo $father_name; ?></h4>
				<h4>Mother: &nbsp; <?php echo $mother_name; ?></h4>				
			</div>
			<div class="span2 pull-left">&nbsp;</div>
			<div class="span6 pull-left">
				<h3><i class="icon-phone icon-orange"></i>&nbsp; <?php echo $phone; ?></h3>
				<h3><i class="icon-mobile-phone icon-purple"></i>&nbsp; <?php echo $mobile; ?></h3>
				<hr>
				<address><i class="icon-home icon-blue"></i>&nbsp; <?php echo $address; ?></address>
			</div>
			</div>
		</div>
		</div>
	</div>
	</div>
	<div id="school-modal" class="modal hide fade" style="display: none;width:44%;">
	<div class="modal-header">
	  <a href="#" class="close" data-dismiss="modal">&times;</a>
	  <h3><i class="icon-home"></i>&nbsp; <?php echo $school_long_name; ?></h3>
	</div>	
	<div class="modal-body">
    <div class="row">	
		<div class="span2">
			<div class="media">
			  <a class="pull-left" href="#">
				<img class="img-polaroid tint" data-src="holder.js/64x64" alt="64x64" src="http://placehold.it/64x64/ff0000/ffffff/">
			  </a>
			</div>
		</div>
		<div class="span10">
			  <small><b>Margrette - Class teacher</b></small>
			  <div class="media-body">
				Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at.
			  </div>
			  <span class="label success">Classes V to VII</span>
		</div>
	</div>
	<hr>
	<div class="row">	
		<div class="span2">
			<div class="media">
			  <a class="pull-left" href="#">
				<img class="img-polaroid tint" data-src="holder.js/64x64" alt="64x64" src="http://placehold.it/64x64/257432/ffffff/">
			  </a>
			</div>  
		</div>
		<div class="span10">
			  <small><b>Jack - Science</b></small>
			  <div class="media-body">
				Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at.
			  </div>
			  <span class="label success">Classes V to VII</span>
		</div>
	</div>
	<hr>
	<div class="row">	
		<div class="span2">
			<div class="media">
			  <a class="pull-left" href="#">
				<img class="img-polaroid tint" data-src="holder.js/64x64" alt="64x64" src="http://placehold.it/64x64/990066/ffffff/">
			  </a>
			</div>  
		</div>
		<div class="span10">
			  <small><b>Lindsey - Mathematics</b></small>
			  <div class="media-body">
				Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at.
			  </div>
			  <span class="label success">Classes V to VII</span>
		</div>
	</div>
	<hr>
	<div class="row">	
		<div class="span2">
			<div class="media">
			  <a class="pull-left" href="#">
				<img class="img-polaroid tint" data-src="holder.js/64x64" alt="64x64" src="http://placehold.it/64x64/3b5998/ffffff/">
			  </a>
			</div>  
		</div>
		<div class="span10">
			  <small><b>Danny - Social studies</b></small>
			  <div class="media-body">
				Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at.
			  </div>
			  <span class="label success">Classes V to VII</span>
		</div>
	</div>
	<hr>
	<div class="row">	
		<div class="span2">
			<div class="media">
			  <a class="pull-left" href="#">
				<img class="img-polaroid tint" data-src="holder.js/64x64" alt="64x64" src="http://placehold.it/64x64/ff0000/ffffff/">
			  </a>
			</div>  
		</div>
		<div class="span10">
			  <small><b>Danny - Social studies</b></small>
			  <div class="media-body">
				Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at.
			  </div>
			  <span class="label success">Classes V to VII</span>
		</div>
	</div>
	</div>
	<div class="modal-footer">
	</div>
	</div>
	<div id="events-modal" class="modal hide fade" style="display: none;">
	<div class="modal-header">
	  <a href="#" class="close" data-dismiss="modal">&times;</a>
	  <h3><i class="icon-tasks"></i>&nbsp;School Calendar</h3>	  
  </div>
	
	<div class="modal-body">
		<div class="span12">
			<iframe src='http://embed.verite.co/timeline/?source=0AtgWbBiDVDgCdGllbmRKTk1aT3ZfSEVVclJBdFZGTFE&font=Bevan-PotanoSans&maptype=toner&lang=en&height=400' width='100%' height='400' frameborder='0'></iframe>
		</div>
	</div>
	<div class="modal-footer">	 
		
	</div>
  </div>
	
	<script src="http://ckrack.github.io/fbootstrapp/js/bootstrap-twipsy.js"></script>
	
	<script>
		$(function () {
		  $("[rel=popover]")
			.popover({
			  offset: 10,
			  placement: 'right'
			})
			.click(function(e) {
			  e.preventDefault()
			})
		});
		
		$(function () {
			$('.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', 'Collapse this branch');
			$('.tree li.parent_li > span').on('click', function (e) {
				var children = $(this).parent('li.parent_li').find(' > ul > li');
				if (children.is(":visible")) {
					children.hide('fast');
					$(this).attr('title', 'Expand this branch').find(' > i').addClass('icon-plus-sign').removeClass('icon-minus-sign');
				} else {
					children.show('fast');
					$(this).attr('title', 'Collapse this branch').find(' > i').addClass('icon-minus-sign').removeClass('icon-plus-sign');
				}
				e.stopPropagation();
			});
		});
	</script>
	  <script src="http://underscorejs.org/underscore-min.js"></script>
	<script type="text/javascript">
		$(function(){

			var bondObjs = {};
			var bondNames = [];

			//get the data to populate the typeahead (plus an id value)
			var throttledRequest = _.debounce(function(query, process){
				//get the data to populate the typeahead (plus an id value)
				$.ajax({
					url: 'js/bonds.json'
					,cache: false
					,success: function(data){
						//reset these containers every time the user searches
						//because we're potentially getting entirely different results from the api
						bondObjs = {};
						bondNames = [];

						//Using underscore.js for a functional approach at looping over the returned data.
						_.each( data, function(item, ix, list){

							//for each iteration of this loop the "item" argument contains
							//1 bond object from the array in our json, such as:
							// { "id":7, "name":"Pierce Brosnan" }

							//add the label to the display array
							bondNames.push( item.name );

							//also store a hashmap so that when bootstrap gives us the selected
							//name we can map that back to an id value
							bondObjs[ item.name ] = item;
						});

						//send the array of results to bootstrap for display
						process( bondNames );
					}
				});
			}, 300);


			$(".search-query").typeahead({
				source: function ( query, process ) {

					//here we pass the query (search) and process callback arguments to the throttled function
					throttledRequest( query, process );

				}
				,highlighter: function( item ){
				  var bond = bondObjs[ item ];
				  
				  return '<div class="bond">'
						+'<img src="' + bond.photo + '" />'+'&nbsp;&nbsp;<strong>' + bond.name + '</strong>'
						+'</div>';
				}
				, updater: function ( selectedName ) {

					//note that the "selectedName" has nothing to do with the markup provided
					//by the highlighter function. It corresponds to the array of names
					//that we sent from the source function.

					//save the id value into the hidden field
					$( "#bondId" ).val( bondObjs[ selectedName ].id );

					//return the string you want to go into the textbox (the name)
					return selectedName;
				}
			});
			
		});
	</script> 