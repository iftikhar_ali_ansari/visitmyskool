<script src="<?php echo base_url(); ?>resources/js/jquery-1.7.min.js">
</script>

<script src="<?php echo base_url();?>resources/js/jQuery.fn.attendance.js"></script>
   <script>
       $(document).ready(function(){
	                   //The default value is loaded here for the calender
	                   $val = '';
					   $active_year = 2013;
					   $min_max_year='';
					   $.get('<?php echo base_url(); ?>index.php/student/max_min_date',
					       function (data,status) {
						       $min_max_year = $.parseJSON(data);
							   //alert(data);
						   }
						   );
					    $.get('<?php echo base_url(); ?>index.php/student/display',
	                        function (data,status) {
	                            $("#announcement").updateAnnouncement($.parseJSON(data));
						    	}
						);  
                            $.get('<?php echo base_url(); ?>index.php/student/fetch_assignment',
	                            function (data,status) {
	                                $("#assignment_table").updateAssignment($.parseJSON(data));
						    	}
						    );  			
					   	    $.ajax({
	                            url: "<?php echo base_url(); ?>index.php/student/attendance",
		                        type: 'get',
		                        datatype: 'html',
		                        success: function(data) {
								alert(data);
								$val = $.parseJSON(data);
							    $("#first_15_date").initialize('date','',$val);
			                    $("#next_remaining_date").initialize('datenext','',$val);
		                        $("#next_morning_hour").initialize('next','',$val);
			                    $("#next_evening_hour").initialize('next','',$val);
			                    $("#first_15_morning_hour").initialize('','',$val);
			                    $("#first_15_evening_hour").initialize('','',$val);
		                            
		                        }
	                        });
					   // it will update the attendance calendar according to the id passed
					   function update_attendance_calendar($ids,$year) {
					        $val = '';
					        $.get('<?php echo base_url(); ?>index.php/student/attendance',
	                            function (data,status) {
	                                $val = $.parseJSON(data);
							        $("#first_15_date").initialize('date',$ids,$val,$year);
			                        $("#next_remaining_date").initialize('datenext',$ids,$val,$year);
			                        $("#next_morning_hour").initialize('next',$ids,$val,$year);
			                        $("#next_evening_hour").initialize('next',$ids,$val,$year);
			                        $("#first_15_morning_hour").initialize('',$ids,$val,$year);
			                        $("#first_15_evening_hour").initialize('',$ids,$val,$year);
	                            }
	                        );
					   }
					   
                // when directly clicking on  the months
			   $(".month").click(function(event) {
			          
					   $ids = $(this).attr('id');
					   // refreshing the active month
					   $("#"+$ids).parent().parent().children("li.active").removeClass("active");
					   $("#"+$ids).parent().addClass('active');
					   $year = parseInt($("#year").children("li.active").text().substr(0,4));
					   //alert("["+$("#year").children("li.active").text().substr(0,4)+"]");
					   update_attendance_calendar($ids,$year);



			   });
			   // when clicked on the previous button
			   $(".prev").click(function(event) {
			       if($("#month").children("li.active").children().attr('id') !='m1') {
				       $ids = $("#month").children("li.active").prev().children().attr('id');
					   // refreshing the active month
					   $("#"+$ids).parent().parent().children("li.active").removeClass("active");
					   $("#"+$ids).parent().addClass('active');
					   $year = parseInt($("#year").children("li.active").text().substr(0,4));
					   update_attendance_calendar($ids,$year);
				   }
			   });
			   // when clicked on the next button
			   $(".next").click(function(event) {
			       if($("#month").children("li.active").children().attr('id') !='m12') {
				       $ids = $("#month").children("li.active").next().children().attr('id');
					   // refreshing the active month
					   $("#"+$ids).parent().parent().children("li.active").removeClass("active");
					   $("#"+$ids).parent().addClass('active');
					   update_attendance_calendar($ids);
				   }
			   });
			   
			   //when clicking on the year button to change the calendar
			   $(".year").click(function(event) {
			       $year = parseInt($(this).text().substr(0,4));
				   $active_year = $year;
				   $("#year").children("li.active").removeClass("active");
				   $(this).parent().addClass("active");
				   update_attendance_calendar('',$year);
			   });
			   
			   //when clicked on the next year button
			   $("#next_year").click(function(event) {
			       
			       if(parseInt($("#year li:first-child").children().text().substr(0,4))<$min_max_year['min']+5) {
				    $y='';
				   $("#year").children().each(function () {
				       $y = parseInt($(this).children().text().substr(0,4));
					   $(this).children().text(($y+1)+"-"+(($y+2)%100));
					   if($y == $active_year) {
					       $(this).addClass("active");
					   }
				   });
				   
				   $active = $("#year").children("li.active");
				   $active.prev().addClass("active");
				   $active.removeClass("active");
				   if($y+1 == $active_year) {
					       $("#year li:last-child").addClass("active");
					   }
				   }
			   });
			   //when clicked on the prev year button
			   $("#prev_year").click(function(event) {
			       if(parseInt($("#year li:first-child").children().text().substr(0,4))>$min_max_year['max']-5) {
				   $("#year").children().each(function () {
				       $y = parseInt($(this).children().text().substr(0,4));
					   $(this).children().text(($y-1)+"-"+(($y)%100));
				   });
				   $y = $("#year li:first-child").children().text().substr(0,4);
				   //alert($y+" in prev_year"+$active_year+" in prev_year");
				   
					   
				   $active = $("#year").children("li.active");
				   $active.next().addClass("active");
				   $active.removeClass("active");
				   if($y == $active_year) {
					       $("#year li:first-child").addClass("active");
					   }
				   }
			   });			       
		   });
   </script>
<div class="page-header">
    <div class="wall-cover">		   
		<img src="<?php echo base_url(); ?>resources/img/profile_pic.jpg" class="img-polaroid tint">
		<span class="standard">VI C</span>
	</div>
</div>
<div class="profile-details">
    <h1 class="name"><a class="modal-link" href="profile-modal" data-controls-modal="profile-modal" data-backdrop="true" data-keyboard="true"><?php echo $student_name; ?></a></h1>
    <h1 class="attendance-status"><a class="modal-link" href="attendance-modal" data-controls-modal="attendance-modal" data-backdrop="true" data-keyboard="true"><i class="icon-calendar icon-purple">&nbsp;</i>&nbsp;<?php print($total_present."/".$total_working_day); ?></a></h1>
		  
    <h1 class="events"><a class="modal-link" href="events-modal" data-controls-modal="events-modal" data-backdrop="true" data-keyboard="true"><i class="icon-tasks icon-yellow"></i>&nbsp;</i>&nbsp; Calendar</a></h1>
	<h1 class="school"><a class="modal-link" href="school-modal" data-controls-modal="school-modal" data-backdrop="true" data-keyboard="true"><i class="icon-home icon-orange"></i>&nbsp;<?php echo $school_short_name; ?></a></h1>
</div> 