          <ul class="nav">
            <li class="<?php echo isActive($pageName,"home")?>"><a href="<?php echo base_url(); ?>index.php/student">Home</a></li>
            <li class="<?php echo isActive($pageName,"progress")?>" ><a href="<?php echo base_url(); ?>index.php/student/view_progress"><i class="icon-bar-chart">&nbsp;</i>&nbsp;Progress</a></li>
			<li class="<?php echo isActive($pageName,"fees")?>"><a href="<?php echo base_url(); ?>index.php/student/view_fees"><i class="icon-money">&nbsp;</i>&nbsp;Fees</a></li>
			<li class="divider-vertical"></li>
		  </ul>
		<form class="navbar-search pull-right">		
		<input type="text" class="search-query span6" placeholder="Search">
		<button class="btn"><i class="icon-signout"></i>&nbsp;Logout</button>
		
		</form>
