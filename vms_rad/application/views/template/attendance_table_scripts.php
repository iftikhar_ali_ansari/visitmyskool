<script src="<?php echo base_url(); ?>resources/js/jquery-1.7.min.js"></script>
	<script src="http://bootsnipp.com/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>resources/js/bootstrap-modal.js"></script>
	<script src="<?php echo base_url(); ?>resources/js/bootstrap-tabs.js"></script>
	<script src="<?php echo base_url(); ?>resources/js/wysihtml5-0.3.0.js"></script>
	<script src="<?php echo base_url(); ?>resources/js/prettify.js"></script>
	<script src="<?php echo base_url(); ?>resources/js/bootstrap-wysihtml5.js"></script>

  <script>
  
  $(document).ready(function() {
  
	$("#assignments").wysihtml5({
		"font-styles": false, //Font styling, e.g. h1, h2, etc. Default true
		"emphasis": false, //Italics, bold, etc. Default true
		"lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
		"html": false, //Button which allows you to edit the generated HTML. Default false
		"link": true, //Button to insert a link. Default true
		"image": true, //Button to insert an image. Default true,
		"color": false //Button to change color of font  
	});
	
	$.ajax({
	    url: "<?php echo base_url(); ?>index.php/teacher/get_staff_detail",
		type: 'get',
		datatype: 'html',
		data: "",
		async: false,
		success: function(data) {
		    $detail = $.parseJSON(data);
			$staff_name = $detail.first_name+" "+$detail.second_name;
			$staff_school_name = $detail.school_name;
		    $("#staff_details").html("<h1 class='name'>"+$staff_name+"</h1>"+
    "<h1 class='school' ><i class='icon-home icon-orange'></i>&nbsp;"+$staff_school_name+"</h1>");
		}
	});
	loadAttendance();
	function loadAttendance($navigate) {
	$date = new Date();
	$current_date = $date.toDateString();
	$current_month = $current_date.substr(4,3);
	$current_day = parseInt($current_date.substr(8,2));
	$number_of_days = new Date($date.getFullYear(),$current_month,0).getDate();
	if($navigate == 'next') {
		
		    if($current_day+5<$number_of_days) {
			    $current_day = $current_day+5;
			}
	}
	if($navigate == 'prev') {
	    if($current_day-5> 0) {
			    $current_day = $current_day-5;
			}
		else {
		    $number_of_days = new Date($date.getFullYear(),$current_month-1,0).getDate();
		    $current_day = $number_of_days+($current_day-5);
		}
	}
	
	$("#attendance_table tr th:not(:nth-child(1), :nth-child(2))").each(function() {
	    $lz = '';
		    $number_of_days = new Date($date.getFullYear(),$current_month,0).getDate();
		    if($current_day> $number_of_days) {
			    $current_day =1;
			}
			
	
	    if($current_day<10) {
		    $lz = '0';
		}
		else {
		    lz='';
		}
	    $(this).text($lz+($current_day++)+"-"+$current_month);
	});
	$start_date = $("#attendance_table tr th:nth-child(3)").text();
	$start_date = parseInt($start_date.substr(0,2));
	if($navigate == 'prev') $start_date -=5;
	
	
	$.ajax({
	    url: "<?php echo base_url(); ?>index.php/teacher/load_attendance_status",
		type: 'get',
		data:{'fclass': $("#mediumselect").val(),'start_date':$start_date },
		datatype: 'html',
		async: false,
		success: function(data) {
			$value = $.parseJSON(data);
			$student_id = $value[0].id;
			$student_name = $value[0].name;
			$tabindex = 2;
			$colindex = 3;
			$attendance_status = "green";
			$.each($value, function($index, $val){
			    if($val.id == $student_id){
				    
				    if($val.status == 1) {
					    $attendance_status = "#257432";
					}
					else {
					    $attendance_status = "#d9230f";
					}
					
				    $("#attendance_table tr:nth-child("+$tabindex+") td:nth-child("+$colindex+")").css("background-color",$attendance_status);
					$colindex++;
					
				}else {
				    $colindex = 3;
					$tabindex++;
				    if($val.id != $student_id){
				    
				    if($val.status == 1) {
					    $attendance_status = "#257432";
						}
					else {
					    $attendance_status = "#d9230f";
						}
					
				    $("#attendance_table tr:nth-child("+$tabindex+") td:nth-child("+$colindex+")").css("background-color",$attendance_status);
					$colindex++;

					$student_name = $val.name;
				    $student_id = $val.id;
				    $("#attendance_table tr:nth-child("+$tabindex+") td:nth-child(1)").text($student_id);
					$("#attendance_table tr:nth-child("+$tabindex+") td:nth-child(2)").text($student_name);
				    
					
				}
				}
				
			});
		    }
	});
	}
	$("#submit_announcement").click(function (){
	    $.ajax({
	    url: "<?php echo base_url(); ?>index.php/teacher/submit_announcement",
		type: 'get',
		data:{"announcement":$("#announcement").val(),"parent_select":$("#announcement_parent_check").is(":checked")},
		datatype: 'html',
		success: function(data) {
		    $("#announcement").val("");
			$("#announcement_parent_check").attr('checked',false);
			alert("successfully updated");
		}
	});
	});
	
	$("#next_week").click(function() {
	    loadAttendance("next");
	});
	
	$("#prev_week").click(function() {
	    
		loadAttendance("prev");
	});
	
	$("#assign_assignment").click(function() {
	    
		$.ajax({
	    url: "<?php echo base_url(); ?>index.php/teacher/submit_assignment",
		type: 'get',
		data:{"assignment":$("#assignments").val(),"class":""},
		datatype: 'html',
		async: false,
		success: function(data) {
		    alert("successfully assigned");
		}
	});
	});
	$('#present_button').click(function() {  
		$('#absent_students option:selected').remove().appendTo('#present_students');
		return false;
	}); 

	$('#absent_button').click(function() {  
		$('#present_students option:selected').remove().appendTo('#absent_students');  
		return false;
	});
	$('#half_day_button').click(function() {  
		$('#present_students option:selected').remove().appendTo('#half_day_students');  
		return false;
	});
	$('#reset_all').click(function(){
		$('#absent_students option').remove().appendTo('#present_students');
		$('#half_day_students option').remove().appendTo('#present_students');
		return false;
	});
	
	
	$(".attendance-table td:not(:first-child, :nth-child(2))").click(function() {
		$(this).css('background','#d9230f');
		return false;
	});
	
	$(".attendance-table td:not(:first-child :nth-child(2))").dblclick(function() {
		$(this).css('background','#257432');
		return false;
	});
	
	$("#saveAttendance").click(function(event) {
	    $(this).saveAttendance(".attendance-table","<?php echo base_url(); ?>index.php/teacher/saveAttendance");
	});
	
	
  });
  </script>