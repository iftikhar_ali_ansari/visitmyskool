<?php echo $basejs?>
<?php echo $header ?>
<div id="main" role="main" class="admin-dashboard container-fluid">
	<div class="sidebar">
	<?php echo $sidebar_content; ?>
	</div>
	<div class="content">
	<?php echo $content_body; ?>
	</div>
</div>
<?php echo $footer ?>
