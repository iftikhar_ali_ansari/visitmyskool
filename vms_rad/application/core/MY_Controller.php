<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller{
	
	//Page info
	protected $data = Array();
	protected $pageName = FALSE;
	protected $template = "login";
	protected $hasNav = True;
	protected $nav = "template/nav";
	protected $enable_blocks = FALSE;
	protected $enable_sidebar = FALSE;
	protected $block = "";
	protected $hasPageHeader = False;
	protected $pageHeader = "";
	protected $hasPageModal = FALSE;
	protected $pageModal = "";
	
	//Page contents
	protected $javascript = array(
		'wysihtml5-0.3.0.js',
		'prettify.js',
		'custom/jquery.dataTables.js',
		'custom/jquery.dataTables.reload.ajax.js',
		'custom/blocks.js'
	);
	protected $css = array(
		'bootstrap-wysihtml5.css',
		'datatable_styles.css',
		'prettify.css',
		'bootstrap.css',
		'bootstrap-custom.css',
		'font-awesome/css/font-awesome.css'	
		);
	protected $fonts = array();
	
	//Page Meta
	protected $title = FALSE;
	protected $body_class = "login";
	protected $description = FALSE;
	protected $keywords = FALSE;
	protected $author = FALSE;
	
	function __construct()
	{	

		parent::__construct();
		$this->data["uri_segment_1"] = $this->uri->segment(1);
		$this->data["uri_segment_2"] = $this->uri->segment(2);
		$this->title = $this->config->item('site_title');
		$this->description = $this->config->item('site_description');
		$this->keywords = $this->config->item('site_keywords');
		$this->author = $this->config->item('site_author');
		
		$this->pageName = strToLower(get_class($this));
	}
	 
	
	protected function _render($view,$renderData="FULLPAGE") {

        switch ($renderData) {
        case "AJAX"     :
            $this->load->view($view,$this->data);
        break;
        case "JSON"     :
            echo json_encode($this->data);
        break;
        case "FULLPAGE" :
        default         : 
		//static
		$toTpl["javascript"] = $this->javascript;
		$toTpl["css"] = $this->css;
		$toTpl["fonts"] = $this->fonts;
		
		//meta
		$toTpl["title"] = $this->title;
		$toTpl["body_class"] = $this->body_class;
		$toTpl["description"] = $this->description;
		$toTpl["keywords"] = $this->keywords;
		$toTpl["author"] = $this->author;
		
		//data
		if( ($this->enable_sidebar == TRUE) and ($this->enable_blocks == TRUE)){
			$toBody["sidebar_content"] = $this->load->view($this->block,array_merge($this->data,$toTpl),true);
		}
		//student page headers
		
		$toTpl["page_modal"] = "";
		if($this->hasPageHeader) {
		    $toBody["page_header"] = $this->load->view($this->pageHeader,$this->data,true);	
		}
		if($this->hasPageModal) {
			$toTpl["page_modal"] = $this->load->view($this->pageModal,'',true);
		}else {
		    $toTpl["page_modal"] = "";
		}
		
		$toBody["content_body"] = $this->load->view($view,array_merge($this->data,$toTpl),true);

		//nav menu
		if($this->hasNav){
			$this->load->helper("nav");
			$toMenu["pageName"] = $this->pageName;
			$toHeader["nav"] = $this->load->view($this->nav,$toMenu,true);
		}
		

		
		
		
		$toHeader["basejs"] = $this->load->view("template/basejs",$this->data,true);		
		$toBody["header"] = $this->load->view("template/header",$toHeader,true);
		$toBody["footer"] = $this->load->view("template/footer",'',true);		
		$toTpl["body"] = $this->load->view("template/".$this->template,$toBody,true);
		
        	
		
		//render view
		$this->load->view("template/skeleton",$toTpl);
		 break;
    }
	}
}
