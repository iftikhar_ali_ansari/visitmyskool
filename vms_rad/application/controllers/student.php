<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Student extends MY_Controller {
		public function __construct()
	{
		parent::__construct();
		$this->load->model("studentmodel");
		$this->load->helper("date_helper");
	}
	public function index() {
		$this->title = "Visitmyskool.com";
		$this->template = "2column_fixed";
		$this->hasNav = True;
		$this->pageName = "home";
		$this->hasPageHeader = True;
		$this->hasPageModal = True;
		$this->pageHeader = "template/student_page_header";
		$this->pageModal = "template/student_page_modal";
		$this->nav = "template/student_nav";
		array_push($this->javascript,"bootstrap-modal.js");
		array_push($this->javascript,"bootstrap-popover.js");
		array_push($this->javascript,"bootstrap-min.js");
		array_push($this->javascript,"jQuery.fn.announcement.js");
		array_push($this->javascript,"jQuery.fn.assignment.js");
		$detail = $this->studentmodel->fetch_student_defaults(4);
		$contact = $this->studentmodel->fetch_contact_detail(4);

		$this->data['student_name']= $detail->first_name." ".$detail->last_name;
		$this->data['father_name']= $detail->father_name;
		$this->data['mother_name']= $detail->mother_name;
		$this->data['class']= 'VI C';
		$this->data['reg_number']= $detail->student_id;
		$this->data['phone']= $contact->landline;
		$this->data['mobile']= $contact->primary_mobile;
		$this->data['address']= $contact->address_1.", ".$contact->address_2.", ".$contact->city.", ".$contact->state.", ".$contact->country;
		
		//school details
		$school_detail = $this->studentmodel->fetch_school_detail(4);
		$this->data['school_long_name'] = $school_detail->long_name;
		$this->data['school_short_name'] = $school_detail->short_name;
		
		//attendance details
		$total_present = $this->studentmodel->fetch_attendance_detail(1);
		$total_days_working = $this->studentmodel->fetch_total_working_day(1);
        $this->data['total_present'] = $total_present->total_present;
		$this->data['total_working_day'] = $total_days_working->total_working_days;
		$this->renderPage($this->data);
		
	}
	
	function view_progress() {
	    echo ' here the progress detail page will display';
	}
	
	function view_fees() {
	    echo 'here the fees detail of the particular student will display';
	}
	
	public function renderPage($data = null) {
		$this->_render('pages/student',$data);
	}
	
	public function display() {
	    $data = $this->studentmodel->fetch_announcement(2);
		echo json_encode($data);
	}
	
	function attendance() {
	    $data = $this->studentmodel->fetch_test_data();
		echo json_encode($data);
   }
   
   function max_min_date() {
       $data = $this->studentmodel->fetch_max_min_date();
	   echo json_encode($data);
   }
   
   function fetch_assignment() {
       $data = $this->studentmodel->fetch_student_assignment();
	   echo json_encode($data);
   }
	
}