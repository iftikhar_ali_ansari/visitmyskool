<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blocks extends MY_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model("adminfeesmodel");
		$this->load->helper("date_helper");
		$this->load->library("datatables");
	}
	
	public function generate_students_table_block() {
		$this->datatables->select("person_id, CONCAT(first_name,' ', last_name) as name",FALSE);
		$this->datatables->from("person");
		$this->datatables->where("person.active_flag","Y");
		echo ($this->datatables->generate());
	}
	
	public function generate_payment_history_table_block($student_id = 10) {
		$this->datatables->select("fee_reciept_code, fee_paid_date, fee_description, fee_amount_recieved",FALSE);
		$this->datatables->from("fee_payment");
		$this->datatables->join("fee_structure_detail","fee_structure_detail.fee_structure_detail_id = fee_payment.fee_structure_detail_id");
		$this->datatables->where("fee_payment.fee_structure_detail_id","fee_structure_detail.fee_structure_detail_id",FALSE);
		$this->datatables->where("fee_payment.student_id",$student_id);
		echo ($this->datatables->generate());
	}
	
}