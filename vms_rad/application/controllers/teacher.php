<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Teacher extends MY_Controller {
		public function __construct()
	{
		parent::__construct();
		$this->load->model("teachermodel");
		$this->load->helper("date_helper");
	}
	
	function index() {
        
	    $this->title="visitmyskool.com";
		$this->template = "2column_fixed";
		$this->body_class = "admin-body";
		$this->pageName = "home";
		Array_push($this->javascript,"jQuery.fn.saveAttendance.js");
		$this->hasPageHeader = TRUE;
		$this->hasNav = TRUE;
		$this->nav = "template/teacher_nav";
		$this->hasPageModal = True;
		$this->pageModal = "template/attendance_table_scripts";
		$this->pageHeader = "header/admin_header";
		$this->_render('pages/teacher/home');
		
	}
	function calendar_for($year,$date) {
	    echo 'the calendar will display here';
	}
	function load_attendance_status() {
	    $class = $this->input->get("fclass");
		$start_date = $this->input->get("start_date");
		
		//$start_date = substr($start_date,0,2);
		//print_r($start_date);
	    $status = $this->teachermodel->fetch_attendance_status($class,$start_date);
		echo json_encode($status);
	}
	function saveAttendance() {
	    $data = $this->input->get("student_name");
		$index = $this->input->get("index");
		//print_r($index);
		$ori = Array();
		$student_id = Array();
		$present_status = Array();
		$month = Array();
		$day = Array();
		$inc = 0;
		$r = 1;
		$m ='';
		$itr = 1;
		$final = Array();
		$query = Array();
		foreach($data as $status) {
		    foreach($status as $val) {
			    ($itr == 1) ? $student_id[] = $val: $present_status[] = $val;
				$itr++;
			}
			$final[] = $present_status;
			$present_status = Array();
			$itr = 1;
			
		}
		
		$index = array_slice($index,2);
		foreach($index as $date) {
		    $day[] = substr($date,0,2);
			$month[] = substr($date,3);
		}
		$increment_day_index = -1;
		for($i = 0; $i<sizeof($student_id); $i++) {
		    for($j = 0;$j<sizeof($index)*2; $j++) {
                $final_query['student_id'] = $student_id[$i];
				if($j%2 == 0) {
				    $increment_day_index++;
					$final_query['shift'] = 0;
				}else {
				    $final_query['shift'] = 1;
				}
				$final_query['attendance_date'] = Date('Y-n-').$day[$increment_day_index];
				$final_query['period_number'] = 0;
				$final_query['Group_id'] = 27;
				($final[$i][$j])?$final_query['attendance_status'] = $final[$i][$j]:$final_query['attendance_status'] = 1;
				$final_query['created_by'] = 1;
				$query[] = $final_query;
			}
			$increment_day_index = -1;
		}
		$this->teachermodel->saveAttendance($query);
	}
	
	function submit_announcement() {
	    $announcement = $this->input->get("announcement");
		$parent_check = $this->input->get("parent_select");
		$this->teachermodel->save_announcement($announcement,$parent_check); //later we will include class in the argument
		
	}
	
	function submit_assignment() {
	    $assignment = $this->input->get("assignment");
		$class = $this->input->get("class");
		$this->teachermodel->save_assignment($assignment);
		
	}
	
	function get_staff_detail($staff_id=2) {
	    $staff_detail = $this->teachermodel->fetch_staff_detail($staff_id);
		echo json_encode($staff_detail);
		//$this->_render('',"JSON");
	}
	
	
}
?>