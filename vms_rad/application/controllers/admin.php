<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MY_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model("adminfeesmodel");
		$this->load->helper("date_helper");
	}
	
	public function index() {
		echo "This would be the admin dashboard; add admin/fees for the fee page.";
	}
	
	public function progress() {
		$this->title="visitmyskool.com";
		$this->template = "1column_fixed";
		$this->body_class = "admin-body";
		$this->pageName = "progress";
		$this->hasPageHeader = TRUE;
		$this->hasPageModal = True;
		$this->pageModal = "template/markstable";
		$this->pageHeader = "header/admin_header";
		$this->data["student_name"] = $this->adminfeesmodel->fetch_student_names();
		$this->data["student_marks"] = $this->adminfeesmodel->fetch_student_marks(10);
		$this->data["staff"] = $this->adminfeesmodel->fetch_staff_details(2);
		$this->data["school_name"] = $this->adminfeesmodel->fetch_school(2);
		$this->data["class"] = $this->adminfeesmodel->fetch_class();
		$this->data["exam_type"] = $this->adminfeesmodel->fetch_exam_type();
		$this->_render('pages/teacher/home');
	}
	
	public function fetch_student_names() {
	    $names = $this->adminfeesmodel->fetch_student_names();
		echo json_encode($names);
	}
	
	public function fees($renderData="") {
    	array_push($this->javascript,'print_table.js');
		array_push($this->javascript,'custom/jquery.handsontable.full.js');
		array_push($this->javascript,'custom/fee_entry_spreadsheet.js');
		array_push($this->javascript,'bootstrap-modal.js');
		array_push($this->javascript,'jquery.min.js');
		//array_push($this->javascript,'google-plus.js');
		array_push($this->css,'jquery.handsontable.css');
		$this->title = "Visitmyskool.com";
		$this->template = "2column_fluid";

		$this->body_class = "admin-body";
		$this->enable_blocks = TRUE;
		$this->enable_sidebar = TRUE;
		$this->block = "blocks/admin_blocks/students_block";
		$this->pageName = "fees";
		$this->data["student_data"] = $this->adminfeesmodel->fetch_student_defaults(10);
		$this->data["next_reciept_no"] = $this->adminfeesmodel->fetch_reciept_no(1);
		$this->data["student_balance_data"] = $this->adminfeesmodel->fetch_student_balance(10);
		$this->data["student_balance_total"] = $this->adminfeesmodel->fetch_student_balance_total(10);
		$this->_render('pages/admin');
	}
	
	public function fetch_student_details() {
		$data = Array();
		$student_id = $this->input->post("student_id");
		$this->data["student_data"] = $this->adminfeesmodel->fetch_student_defaults($student_id);
		$this->_render('pages/admin',"JSON");
	}
	
	public function fetch_pending_payments() {
		$data = array();
		$student_id = $this->input->post("student_id");
		$pending_payments_array = $this->adminfeesmodel->fetch_pending_payments($student_id);
		$pending_payments_array_length = count($pending_payments_array);
		$pending_payments_array_counter = 0;
		while($pending_payments_array_counter < $pending_payments_array_length) {
			array_push($data, $pending_payments_array[$pending_payments_array_counter]["fee_structure_detail_id"]);
			array_push($data, $pending_payments_array[$pending_payments_array_counter]["fee_description"]);
			array_push($data, $pending_payments_array[$pending_payments_array_counter]["fee_amount"]);
			$pending_payments_array_counter++;
		}
		echo json_encode($data);
	}
	
	public function save_fee_entry() {
		$fee_payment_data = array();
		$insert_result_flag = "error";
		$row_index = 0;
		$col_index = 0;
		$json_input_data = $this->input->post("data");
		$total_no_of_rows = count($json_input_data);
		$fee_payment_data["fee_reciept_code"] = $this->adminfeesmodel->fetch_reciept_no(1);
		if($total_no_of_rows > 0){
			if($total_no_of_rows > 0) {
				while ($row_index < $total_no_of_rows-1) {
					$total_no_of_cols  = count($json_input_data[$row_index]);
					while($col_index <= $total_no_of_cols-1) {
						if($col_index == 0) {
							$fee_structure_detail_id = $json_input_data[$row_index][$col_index];							
						} else if($col_index == 2)	{
							$fee_amount_recieved = $json_input_data[$row_index][$col_index];
						}
						$col_index++;
					}
					$col_index = 0;
					
					$fee_payment_data["student_id"] = 10;
					$fee_payment_data["school_id"] = 1;
					$fee_payment_data["school_branch_id"] = 1;
					$fee_payment_data["fee_structure_detail_id"] = $fee_structure_detail_id;
					$fee_payment_data["fee_amount_recieved"] = $fee_amount_recieved;
					$fee_payment_data["term_id"] = 1;
					$fee_payment_data["fee_paid_date"] = get_today_date();
					$fee_payment_data["updated_by"] = 101;					
					if($this->adminfeesmodel->save_fee_payment($fee_payment_data)) {
						$insert_result_flag = "ok";
					} else {
						$insert_result_flag = "error";
						break;
					}
					
					$row_index++;
				}
			}
		}
		$data = array("result"=> $insert_result_flag);
		echo json_encode($data);
	}
	
	public function create() {

	$data=array(
	'First_Name' => $this-> input-> post('First_Name'),
	'Last_Name' => $this-> input-> post('Last_Name'),
	'Student_Email_Id' => $this->input->post('Student_Email_Id'),
	'Father_Name' =>$this->input->post('Father_Name'),
	'Father_Email_Id' => $this->input->post('Father_Email_Id'),
	'Father_Mobile_No' =>$this->input->post('Father_Mobile_No'),
	'Address' => $this->input->post('Address'),
	'Locality' =>$this->input->post('Locality'),
	'PIN_Code' => $this->input->post('PIN_Code'),
	'Middle_Name' =>$this->input->post('Middle_Name'),
	'Class' => $this->input->post('Class_'),
	'Section' =>$this->input->post('Section'),
	'DOB' => $this->input->post('DOB'),
	'Mother_Name' =>$this->input->post('Mother_Name'),
	'Mother_Email_Id' => $this->input->post('Mother_Email_Id'),
	'Mother_Mobile_No' =>$this->input->post('Mother_Mobile_No'),
	'City' => $this->input->post('City'),
	'Country' =>$this->input->post('Country')
	
	);
	 $this -> adminfeesmodel-> add_reccord($data); 

   	if($this->input->post('ajax'))
		{

		$this->load->view("thanku");
		}
else{
 
	echo "YOUR DETAILS HAVE BEEN RECORDED";
	
}

	
}
	
	
}