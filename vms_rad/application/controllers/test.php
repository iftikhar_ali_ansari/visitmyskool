<?php 
class Test extends MY_Controller {
    public function __construct()
	{
		parent::__construct();
		$this->load->model("studentmodel");
		$this->load->helper("date_helper");
	}
	function paging() {
	    $data = $this->studentmodel->fetch_test_data();
		echo json_encode($data);
   }
   function display() {
       $this->load->view('testpage');
   }
}
?>