<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('googleplus');
	}
	
	public function index() {		
		$this->load->view('login');
	}
	
	public function login_state() {
		print_r($this->input->post());
	}
	
	public function people() {
		print_r($this->input->post());
		redirect("admin/fees");
	}
	
}